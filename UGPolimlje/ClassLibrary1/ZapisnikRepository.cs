﻿using DataLayer.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class ZapisnikRepository
    {
        static private string MySqlConnectionstring = ConfigurationManager.ConnectionStrings["connstring"].ConnectionString;
        static private MySqlConnection databaseConnection = new MySqlConnection(MySqlConnectionstring);

        public DataTable Selektovanje(string kolone, string uslov)
        {
            //Pravljenje tabele kojoj ce biti prosledjene vrednosti iz baze
            DataTable dt = new DataTable();

            try
            {
                //Sql komanda za uzimanje podataka iz baze
                string sql = "SELECT " + kolone + " FROM zapisnici " + uslov;
                //Izvrsavanje Sql komande
                MySqlCommand command = new MySqlCommand(sql, databaseConnection);
                //Popunjavanje tabele podacima
                MySqlDataAdapter adapter = new MySqlDataAdapter(command);
                databaseConnection.Open();
                adapter.Fill(dt);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                databaseConnection.Close();
            }
            return dt;
        }

        public bool UnosZapisnika(Zapisnik zapisnik)
        {

            bool isSucces = false;

            try
            {
                //Sql komanda za unos podataka u baze
                string sql = "INSERT INTO zapisnici VALUES (@datumUtakmice,@redniBroj,@oblacnost,@pravacIJacinaVetra,@vazdusniPritisak,@temperatura,@primedbeTakmicara,@primedbePrvogSudije,@primedbeDrugogSudije)";

                //Izvrsavanje Sql komande
                MySqlCommand cmd = new MySqlCommand(sql, databaseConnection);
                cmd.Parameters.AddWithValue("@datumUtakmice", zapisnik.DatumUtakmice);
                cmd.Parameters.AddWithValue("@redniBroj", zapisnik.RedniBrojUtakmice);
                cmd.Parameters.AddWithValue("@oblacnost", zapisnik.Oblacnost);
                cmd.Parameters.AddWithValue("@pravacIJacinaVetra", zapisnik.PravacIJacinaVetra);
                cmd.Parameters.AddWithValue("@vazdusniPritisak", zapisnik.VazdusniPritisak);
                cmd.Parameters.AddWithValue("@temperatura", zapisnik.Temperatura);
                cmd.Parameters.AddWithValue("@primedbeTakmicara", zapisnik.PrimedbeTakmicara);
                cmd.Parameters.AddWithValue("@primedbePrvogSudije", zapisnik.PrimedbePrvogSudije);
                cmd.Parameters.AddWithValue("@primedbeDrugogSudije", zapisnik.PrimedbeDrugogSudije);

                databaseConnection.Open();
                int rows = cmd.ExecuteNonQuery();
                // Ako se izvrsi uspesno vrednost  rows  > 0
                if (rows > 0)
                {
                    isSucces = true;
                }
                else
                {
                    isSucces = false;
                }

            }

            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            finally
            {
                databaseConnection.Close();
            }
            return isSucces;
        }

        //Metoda za azuriranje podataka u bazi
        public bool AzuriranjeZapisnika(Zapisnik zapisnik)
        {
            bool isSucces = false;

            try
            {
                string sql = "UPDATE zapisnici SET oblacnost=@oblacnost,pravacIJacinaVetra=@pravacIJacinaVetra,vazdusniPritisak=@vazdusniPritisak,temperatura=@temperatura,primedbeTakmicara=@primedbeTakmicara,PrimedbeSudije1=@primedbePrvogSudije,PrimedbeSudije2=@primedbeDrugogSudije WHERE DatumUtakmice=@datumUtakmice AND Rbr_utakmice=@redniBroj";

                //Izvrsavanje Sql komande
                MySqlCommand cmd = new MySqlCommand(sql, databaseConnection);
                cmd.Parameters.AddWithValue("@datumUtakmice", zapisnik.DatumUtakmice);
                cmd.Parameters.AddWithValue("@redniBroj", zapisnik.RedniBrojUtakmice);
                cmd.Parameters.AddWithValue("@oblacnost", zapisnik.Oblacnost);
                cmd.Parameters.AddWithValue("@pravacIJacinaVetra", zapisnik.PravacIJacinaVetra);
                cmd.Parameters.AddWithValue("@vazdusniPritisak", zapisnik.VazdusniPritisak);
                cmd.Parameters.AddWithValue("@temperatura", zapisnik.Temperatura);
                cmd.Parameters.AddWithValue("@primedbeTakmicara", zapisnik.PrimedbeTakmicara);
                cmd.Parameters.AddWithValue("@primedbePrvogSudije", zapisnik.PrimedbePrvogSudije);
                cmd.Parameters.AddWithValue("@primedbeDrugogSudije", zapisnik.PrimedbeDrugogSudije);

                databaseConnection.Open();
                int rows = cmd.ExecuteNonQuery();
                // Ako se izvrsi uspesno vrednost  rows  > 0
                if (rows > 0)
                {
                    isSucces = true;
                }
                else
                {
                    isSucces = false;
                }

            }

            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("" + ex);
            }

            finally
            {
                databaseConnection.Close();
            }
            return isSucces;
        }

        //Metoda za brisanje podataka u bazi
        public bool BrisanjeZapisnika(string datumOdrzavanja, int redniBrojUtakmice)
        {
            bool isSucces = false;

            try
            {
                //Sql komanda za brisanje podataka u baze
                string sql = "DELETE FROM zapisnici WHERE DatumUtakmice=@datumUtakmice AND Rbr_utakmice=@redniBroj";

                //Izvrsavanje Sql komande
                MySqlCommand cmd = new MySqlCommand(sql, databaseConnection);
                cmd.Parameters.AddWithValue("@datumUtakmice", datumOdrzavanja);
                cmd.Parameters.AddWithValue("@redniBroj", redniBrojUtakmice);

                databaseConnection.Open();
                int rows = cmd.ExecuteNonQuery();
                // Ako se izvrsi uspesno vrednost  rows  > 0
                if (rows > 0)
                {
                    isSucces = true;
                }
                else
                {
                    isSucces = false;
                }

            }

            catch (Exception ex)
            {
            }

            finally
            {
                databaseConnection.Close();
            }
            return isSucces;

        }
    }
}
