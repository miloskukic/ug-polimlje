﻿using DataLayer.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class GolubRepository
    {

        static private string MySqlConnectionstring = ConfigurationManager.ConnectionStrings["connstring"].ConnectionString;
        static private MySqlConnection databaseConnection = new MySqlConnection(MySqlConnectionstring);

        public DataTable Selektovanje(string kolone, string uslov)
        {
            //Pravljenje tabele kojoj ce biti prosledjene vrednosti iz baze
            DataTable dt = new DataTable();

            try
            {
                //Sql komanda za uzimanje podataka iz baze
                string sql = "SELECT " + kolone + " FROM golubovi " + uslov;
                //Izvrsavanje Sql komande
                MySqlCommand command = new MySqlCommand(sql, databaseConnection);
                //Popunjavanje tabele podacima
                MySqlDataAdapter adapter = new MySqlDataAdapter(command);
                databaseConnection.Open();
                adapter.Fill(dt);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                databaseConnection.Close();
            }
            return dt;
        }

        public bool UnosGoluba(Golub golub)
        {

            bool isSucces = false;

            try
            {
                //Sql komanda za unos podataka u baze
                string sql = "INSERT INTO golubovi VALUES (@id,@idVlasnika,@pol,@idVrste,@opis)";

                //Izvrsavanje Sql komande
                MySqlCommand cmd = new MySqlCommand(sql, databaseConnection);
                cmd.Parameters.AddWithValue("@id", golub.Id);
                cmd.Parameters.AddWithValue("@idVlasnika", golub.IdVlasnika);
                cmd.Parameters.AddWithValue("@idVrste", golub.IdVrste);
                cmd.Parameters.AddWithValue("@pol", golub.Pol);
                cmd.Parameters.AddWithValue("@opis", golub.Opis);

                databaseConnection.Open();
                int rows = cmd.ExecuteNonQuery();
                // Ako se izvrsi uspesno vrednost  rows  > 0
                if (rows > 0)
                {
                    isSucces = true;
                }
                else
                {
                    isSucces = false;
                }

            }

            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            finally
            {
                databaseConnection.Close();
            }
            return isSucces;
        }

        //Metoda za azuriranje podataka u bazi
        public bool AzuriranjeGoluba(Golub golub)
        {
            bool isSucces = false;

            try
            {
                string sql = "UPDATE golubovi SET idVlasnika=@idVlasnika,id_vrste=@idVrste,pol=@pol,opis=@opis WHERE Id=@id";

                //Izvrsavanje Sql komande
                MySqlCommand cmd = new MySqlCommand(sql, databaseConnection);
                cmd.Parameters.AddWithValue("@id", golub.Id);
                cmd.Parameters.AddWithValue("@idVlasnika", golub.IdVlasnika);
                cmd.Parameters.AddWithValue("@idVrste", golub.IdVrste);
                cmd.Parameters.AddWithValue("@pol", golub.Pol);
                cmd.Parameters.AddWithValue("@opis", golub.Opis);

                databaseConnection.Open();
                int rows = cmd.ExecuteNonQuery();
                // Ako se izvrsi uspesno vrednost  rows  > 0
                if (rows > 0)
                {
                    isSucces = true;
                }
                else
                {
                    isSucces = false;
                }

            }

            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("" + ex);
            }

            finally
            {
                databaseConnection.Close();
            }
            return isSucces;
        }

        //Metoda za brisanje podataka u bazi
        public bool BrisanjeGoluba(string idGoluba)
        {
            bool isSucces = false;

            try
            {
                //Sql komanda za brisanje podataka u baze
                string sql = "DELETE FROM golubovi WHERE Id=@id";

                //Izvrsavanje Sql komande
                MySqlCommand cmd = new MySqlCommand(sql, databaseConnection);

                cmd.Parameters.AddWithValue("@id", idGoluba);

                databaseConnection.Open();
                int rows = cmd.ExecuteNonQuery();
                // Ako se izvrsi uspesno vrednost  rows  > 0
                if (rows > 0)
                {
                    isSucces = true;
                }
                else
                {
                    isSucces = false;
                }

            }

            catch (Exception ex)
            {
            }

            finally
            {
                databaseConnection.Close();
            }
            return isSucces;

        }


    }
}
