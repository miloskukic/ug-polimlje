﻿using DataLayer.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class OpisRepository
    {
        static private string MySqlConnectionstring = ConfigurationManager.ConnectionStrings["connstring"].ConnectionString;
        static private MySqlConnection databaseConnection = new MySqlConnection(MySqlConnectionstring);

        public DataTable Selektovanje(string kolone, string uslov)
        {
            //Pravljenje tabele kojoj ce biti prosledjene vrednosti iz baze
            DataTable dt = new DataTable();

            try
            {
                //Sql komanda za uzimanje podataka iz baze
                string sql = "SELECT " + kolone + " FROM opisi " + uslov;
                //Izvrsavanje Sql komande
                MySqlCommand command = new MySqlCommand(sql, databaseConnection);
                //Popunjavanje tabele podacima
                MySqlDataAdapter adapter = new MySqlDataAdapter(command);
                databaseConnection.Open();
                adapter.Fill(dt);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                databaseConnection.Close();
            }
            return dt;
        }

        public bool UnosOpisa(Opis opis)
        {

            bool isSucces = false;

            try
            {
                //Sql komanda za unos podataka u baze
                string sql = "INSERT INTO opisi VALUES (@sat,@redniBroj,@datumUtakmice,@dodatniOpis)";

                //Izvrsavanje Sql komande
                MySqlCommand cmd = new MySqlCommand(sql, databaseConnection);
                cmd.Parameters.AddWithValue("@sat", opis.Vreme);
                cmd.Parameters.AddWithValue("@redniBroj", opis.RedniBrojUtakmice);
                cmd.Parameters.AddWithValue("@datumUtakmice", opis.DatumUtakmice);
                cmd.Parameters.AddWithValue("@dodatniOpis", opis.DodatniOpis);

                databaseConnection.Open();
                int rows = cmd.ExecuteNonQuery();
                // Ako se izvrsi uspesno vrednost  rows  > 0
                if (rows > 0)
                {
                    isSucces = true;
                }
                else
                {
                    isSucces = false;
                }

            }

            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            finally
            {
                databaseConnection.Close();
            }
            return isSucces;
        }

        //Metoda za azuriranje podataka u bazi
        public bool AzuriranjeOpisa(Opis opis)
        {
            bool isSucces = false;

            try
            {
                string sql = "UPDATE opisi SET vreme=@vreme,rbr_utakmice=@redniBroj,datumUtakmice=@datumUtakmice,opis=@dodatniOpis WHERE datumUtakmice=@datumUtakmice AND rbr_utakmice=@redniBroj AND vreme=@vreme";

                //Izvrsavanje Sql komande
                MySqlCommand cmd = new MySqlCommand(sql, databaseConnection);
                cmd.Parameters.AddWithValue("@vreme", opis.Vreme);
                cmd.Parameters.AddWithValue("@redniBroj", opis.RedniBrojUtakmice);
                cmd.Parameters.AddWithValue("@datumUtakmice", opis.DatumUtakmice);
                cmd.Parameters.AddWithValue("@dodatniOpis", opis.DodatniOpis);

                databaseConnection.Open();
                int rows = cmd.ExecuteNonQuery();
                // Ako se izvrsi uspesno vrednost  rows  > 0
                if (rows > 0)
                {
                    isSucces = true;
                }
                else
                {
                    isSucces = false;
                }

            }

            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("" + ex);
            }

            finally
            {
                databaseConnection.Close();
            }
            return isSucces;
        }

        //Metoda za brisanje podataka u bazi
        public bool BrisanjeOpisa(string vreme, int redniBrojUtakmice, string datumOdrzavanja)
        {
            bool isSucces = false;

            try
            {
                //Sql komanda za brisanje podataka u baze
                string sql = "DELETE FROM opisi WHERE vreme=@sat AND datumUtakmice=@datumUtakmice AND rbr_utakmice=@redniBroj";

                //Izvrsavanje Sql komande
                MySqlCommand cmd = new MySqlCommand(sql, databaseConnection);
                cmd.Parameters.AddWithValue("@sat", vreme);
                cmd.Parameters.AddWithValue("@datumUtakmice", datumOdrzavanja);
                cmd.Parameters.AddWithValue("@redniBroj", redniBrojUtakmice);

                databaseConnection.Open();
                int rows = cmd.ExecuteNonQuery();
                // Ako se izvrsi uspesno vrednost  rows  > 0
                if (rows > 0)
                {
                    isSucces = true;
                }
                else
                {
                    isSucces = false;
                }

            }

            catch (Exception ex)
            {
            }

            finally
            {
                databaseConnection.Close();
            }
            return isSucces;

        }
    }
}
