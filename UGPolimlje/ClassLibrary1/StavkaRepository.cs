﻿using DataLayer.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class StavkaRepository
    {
        static private string MySqlConnectionstring = ConfigurationManager.ConnectionStrings["connstring"].ConnectionString;
        static private MySqlConnection databaseConnection = new MySqlConnection(MySqlConnectionstring);

        public DataTable Selektovanje(string kolone, string uslov)
        {
            //Pravljenje tabele kojoj ce biti prosledjene vrednosti iz baze
            DataTable dt = new DataTable();

            try
            {
                //Sql komanda za uzimanje podataka iz baze
                string sql = "SELECT " + kolone + " FROM stavke " + uslov;
                //Izvrsavanje Sql komande
                MySqlCommand command = new MySqlCommand(sql, databaseConnection);
                //Popunjavanje tabele podacima
                MySqlDataAdapter adapter = new MySqlDataAdapter(command);
                databaseConnection.Open();
                adapter.Fill(dt);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                databaseConnection.Close();
            }
            return dt;
        }

        public bool UnosStavke(Stavka stavka)
        {

            bool isSucces = false;

            try
            {
                //Sql komanda za unos podataka u baze
                string sql = "INSERT INTO stavke VALUES (@datumUtakmice,@redniBroj,@idGoluba,@bojaAlke,@vremePoletanja,@vremeSletanja,@leteoUPropisnomVisu,@napomena)";

                //Izvrsavanje Sql komande
                MySqlCommand cmd = new MySqlCommand(sql, databaseConnection);
                cmd.Parameters.AddWithValue("@datumUtakmice", stavka.DatumUtakmice);
                cmd.Parameters.AddWithValue("@redniBroj", stavka.RedniBrojUtakmice);
                cmd.Parameters.AddWithValue("@idGoluba", stavka.IdGoluba);
                cmd.Parameters.AddWithValue("@bojaAlke", stavka.BojaAlke);
                cmd.Parameters.AddWithValue("@vremePoletanja", stavka.VremePoletanja);
                cmd.Parameters.AddWithValue("@vremeSletanja", stavka.VremeSletanja);
                cmd.Parameters.AddWithValue("@leteoUPropisnomVisu", stavka.LeteoUPropisnomVisu);
                cmd.Parameters.AddWithValue("@napomena", stavka.Napomena);

                databaseConnection.Open();
                int rows = cmd.ExecuteNonQuery();
                // Ako se izvrsi uspesno vrednost  rows  > 0
                if (rows > 0)
                {
                    isSucces = true;
                }
                else
                {
                    isSucces = false;
                }

            }

            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            finally
            {
                databaseConnection.Close();
            }
            return isSucces;
        }

        //Metoda za azuriranje podataka u bazi
        public bool AzuriranjeStavke(Stavka stavka)
        {
            bool isSucces = false;

            try
            {
                string sql = "UPDATE stavke SET bojaAlke=@bojaAlke,VremePoletanja=@vremePoletanja,VremeSletanja=@vremeSletanja,LeteoUPropisnomVisu=@leteoUPropisnomVisu,Napomena=@napomena WHERE DatumUtakmice=@datumUtakmice AND Rbr_utakmice=@redniBroj AND Id_goluba=@idGoluba";

                //Izvrsavanje Sql komande
                MySqlCommand cmd = new MySqlCommand(sql, databaseConnection);
                cmd.Parameters.AddWithValue("@datumUtakmice", stavka.DatumUtakmice);
                cmd.Parameters.AddWithValue("@redniBroj", stavka.RedniBrojUtakmice);
                cmd.Parameters.AddWithValue("@idGoluba", stavka.IdGoluba);
                cmd.Parameters.AddWithValue("@bojaAlke", stavka.BojaAlke);
                cmd.Parameters.AddWithValue("@vremePoletanja", stavka.VremePoletanja);
                cmd.Parameters.AddWithValue("@vremeSletanja", stavka.VremeSletanja);
                cmd.Parameters.AddWithValue("@leteoUPropisnomVisu", stavka.LeteoUPropisnomVisu);
                cmd.Parameters.AddWithValue("@napomena", stavka.Napomena);

                databaseConnection.Open();
                int rows = cmd.ExecuteNonQuery();
                // Ako se izvrsi uspesno vrednost  rows  > 0
                if (rows > 0)
                {
                    isSucces = true;
                }
                else
                {
                    isSucces = false;
                }

            }

            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("" + ex);
            }

            finally
            {
                databaseConnection.Close();
            }
            return isSucces;
        }

        //Metoda za brisanje podataka u bazi
        public bool BrisanjeStavke(string datumOdrzavanja, int redniBrojUtakmice, int idGoluba)
        {
            bool isSucces = false;

            try
            {
                //Sql komanda za brisanje podataka u baze
                string sql = "DELETE FROM stavke WHERE DatumUtakmice=@datumUtakmice AND Rbr_utakmice=@redniBroj AND Id_goluba=@idGoluba";

                //Izvrsavanje Sql komande
                MySqlCommand cmd = new MySqlCommand(sql, databaseConnection);
                cmd.Parameters.AddWithValue("@datumUtakmice", datumOdrzavanja);
                cmd.Parameters.AddWithValue("@redniBroj", redniBrojUtakmice);
                cmd.Parameters.AddWithValue("@idGoluba", idGoluba);

                databaseConnection.Open();
                int rows = cmd.ExecuteNonQuery();
                // Ako se izvrsi uspesno vrednost  rows  > 0
                if (rows > 0)
                {
                    isSucces = true;
                }
                else
                {
                    isSucces = false;
                }

            }

            catch (Exception ex)
            {
            }

            finally
            {
                databaseConnection.Close();
            }
            return isSucces;

        }
    }
}
