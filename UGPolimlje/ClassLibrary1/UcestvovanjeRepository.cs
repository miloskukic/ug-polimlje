﻿using DataLayer.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class UcestvovanjeRepository
    {
        static private string MySqlConnectionstring = ConfigurationManager.ConnectionStrings["connstring"].ConnectionString;
        static private MySqlConnection databaseConnection = new MySqlConnection(MySqlConnectionstring);

        public DataTable Selektovanje(string kolone, string uslov)
        {
            //Pravljenje tabele kojoj ce biti prosledjene vrednosti iz baze
            DataTable dt = new DataTable();

            try
            {
                //Sql komanda za uzimanje podataka iz baze
                string sql = "SELECT " + kolone + " FROM ucestvovanja " + uslov;
                //Izvrsavanje Sql komande
                MySqlCommand command = new MySqlCommand(sql, databaseConnection);
                //Popunjavanje tabele podacima
                MySqlDataAdapter adapter = new MySqlDataAdapter(command);
                databaseConnection.Open();
                adapter.Fill(dt);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                databaseConnection.Close();
            }
            return dt;
        }

        public bool UnosUcestvovanja(Ucestvovanje ucestvovanje)
        {

            bool isSucces = false;

            try
            {
                //Sql komanda za unos podataka u baze
                string sql = "INSERT INTO ucestvovanja VALUES (@idClana,@datumOdrzavanjaUtakmice,@redniBrojUtakmice,@uloga,@napomena)";

                //Izvrsavanje Sql komande
                MySqlCommand cmd = new MySqlCommand(sql, databaseConnection);
                cmd.Parameters.AddWithValue("@idClana", ucestvovanje.IdClana);
                cmd.Parameters.AddWithValue("@datumOdrzavanjaUtakmice", ucestvovanje.DatumOdrzavanjaUtakmice);
                cmd.Parameters.AddWithValue("@redniBrojUtakmice", ucestvovanje.RedniBrojUtakmice);
                cmd.Parameters.AddWithValue("@uloga", ucestvovanje.Uloga);
                cmd.Parameters.AddWithValue("@napomena", ucestvovanje.Napomena);

                databaseConnection.Open();
                int rows = cmd.ExecuteNonQuery();
                // Ako se izvrsi uspesno vrednost  rows  > 0
                if (rows > 0)
                {
                    isSucces = true;
                }
                else
                {
                    isSucces = false;
                }

            }

            catch (Exception ex)
            {
            }

            finally
            {
                databaseConnection.Close();
            }
            return isSucces;
        }

        //Metoda za brisanje podataka u bazi
        public bool BrisanjeUcestvovanja(int idClana, int redniBrojUtakmice, string datumOdrzavanja)
        {
            bool isSucces = false;

            try
            {
                //Sql komanda za brisanje podataka u baze
                string sql = "DELETE FROM ucestvovanja WHERE id_clana=@idClana AND Rbr_utakmice=@redniBrojUtakmice AND DatumUtakmice=@datumOdrzavanja";
                
                //Izvrsavanje Sql komande
                MySqlCommand cmd = new MySqlCommand(sql, databaseConnection);

                cmd.Parameters.AddWithValue("@idClana", idClana);
                cmd.Parameters.AddWithValue("@redniBrojUtakmice", redniBrojUtakmice);
                cmd.Parameters.AddWithValue("@datumOdrzavanja", datumOdrzavanja);

                databaseConnection.Open();
                int rows = cmd.ExecuteNonQuery();
                // Ako se izvrsi uspesno vrednost  rows  > 0
                if (rows > 0)
                {
                    isSucces = true;
                }
                else
                {
                    isSucces = false;
                }

            }

            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            finally
            {
                databaseConnection.Close();
            }
            return isSucces;

        }
    }
}
