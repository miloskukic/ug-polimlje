﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Stavka
    {
        private String datumUtakmice;
        private int redniBrojUtakmice;
        private string idGoluba;
        private string bojaAlke;
        private string vremePoletanja;
        private string vremeSletanja;
        private string leteoUPropisnomVisu;
        private string napomena;

        public Stavka(String datumUtakmice, int redniBrojUtakmice, string idGoluba, string bojaAlke, string vremePoletanja, string vremeSletanja, string leteoUPropisnomVisu, string napomena)
        {
            this.datumUtakmice = datumUtakmice;
            this.redniBrojUtakmice = redniBrojUtakmice;
            this.idGoluba = idGoluba;
            this.bojaAlke = bojaAlke;
            this.vremePoletanja = vremePoletanja;
            this.vremeSletanja = vremeSletanja;
            this.leteoUPropisnomVisu = leteoUPropisnomVisu;
            this.napomena = napomena;
        }

        public String DatumUtakmice { get => datumUtakmice; set => datumUtakmice = value; }
        public int RedniBrojUtakmice { get => redniBrojUtakmice; set => redniBrojUtakmice = value; }
        public string IdGoluba { get => idGoluba; set => idGoluba = value; }
        public string BojaAlke { get => bojaAlke; set => bojaAlke = value; }
        public string VremePoletanja { get => vremePoletanja; set => vremePoletanja = value; }
        public string VremeSletanja { get => vremeSletanja; set => vremeSletanja = value; }
        public string LeteoUPropisnomVisu { get => leteoUPropisnomVisu; set => leteoUPropisnomVisu = value; }
        public string Napomena { get => napomena; set => napomena = value; }
    }
}
