﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Ucestvovanje
    {
        private int idClana;
        private String datumOdrzavanjaUtakmice;
        private int redniBrojUtakmice;
        private string uloga;
        private string napomena;

        public Ucestvovanje(int idClana, String datumOdrzavanjaUtakmice, int redniBrojUtakmice, string uloga, string napomena)
        {
            this.idClana = idClana;
            this.datumOdrzavanjaUtakmice = datumOdrzavanjaUtakmice;
            this.redniBrojUtakmice = redniBrojUtakmice;
            this.uloga = uloga;
            this.napomena = napomena;
        }

        public int IdClana { get => idClana; set => idClana = value; }
        public String DatumOdrzavanjaUtakmice { get => datumOdrzavanjaUtakmice; set => datumOdrzavanjaUtakmice = value; }
        public int RedniBrojUtakmice { get => redniBrojUtakmice; set => redniBrojUtakmice = value; }
        public string Uloga { get => uloga; set => uloga = value; }
        public string Napomena { get => napomena; set => napomena = value; }
    }
}
