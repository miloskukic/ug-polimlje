﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Clan
    {
        private int id;
        private string ime;
        private string prezime;
        private string jmbg;
        private string brojLicneKarte;
        private string adresa;
        private String datumRodjenja;
        private string telefon;
        private string telefon2;
        private string email;
        private string brojSudijskeLegitimacije;
        private string pol;
        private string datumUclanjenja;

        public Clan(int id, string ime, string prezime, string jmbg,
            string brojLicneKarte, string adresa,String datumRodjenja,
            string telefon, string telefon2, string email,
            string brojSudijskeLegitimacije, string pol, string datumUclanjenja)
        {
            this.id = id;
            this.ime = ime;
            this.prezime = prezime;
            this.jmbg = jmbg;
            this.brojLicneKarte = brojLicneKarte;
            this.adresa = adresa;
            this.datumRodjenja = datumRodjenja;
            this.telefon = telefon;
            this.telefon2 = telefon2;
            this.email = email;
            this.brojSudijskeLegitimacije = brojSudijskeLegitimacije;
            this.pol = pol;
            this.datumUclanjenja = datumUclanjenja;
        }

        public int Id { get => id; set => id = value; }
        public string Ime { get => ime; set => ime = value; }
        public string Prezime { get => prezime; set => prezime = value; }
        public string Jmbg { get => jmbg; set => jmbg = value; }
        public string BrojLicneKarte { get => brojLicneKarte; set => brojLicneKarte = value; }
        public string Adresa { get => adresa; set => adresa = value; }
        public String DatumRodjenja { get => datumRodjenja; set => datumRodjenja = value; }
        public string Telefon { get => telefon; set => telefon = value; }
        public string Telefon2 { get => telefon2; set => telefon2 = value; }
        public string Email { get => email; set => email = value; }
        public string BrojSudijskeLegitimacije { get => brojSudijskeLegitimacije; set => brojSudijskeLegitimacije = value; }
        public string Pol { get => pol; set => pol = value; }
        public string DatumUclanjenja { get => datumUclanjenja; set => datumUclanjenja = value; }
    }
}
