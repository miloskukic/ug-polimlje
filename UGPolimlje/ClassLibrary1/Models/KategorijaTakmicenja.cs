﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class KategorijaTakmicenja
    {
        private int id;
        private string naziv;

        public KategorijaTakmicenja(int id, string naziv)
        {
            this.id = id;
            this.naziv = naziv;
        }

        public int Id { get => id; set => id = value; }
        public string Naziv { get => naziv; set => naziv = value; }
    }
}
