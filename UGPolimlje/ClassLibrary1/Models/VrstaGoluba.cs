﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class VrstaGoluba
    {
        private int id;
        private string naziv;

        public VrstaGoluba(int id, string naziv)
        {
            this.id = id;
            this.naziv = naziv;
        }

        public int Id { get => id; set => id = value; }
        public string Naziv { get => naziv; set => naziv = value; }
    }
}
