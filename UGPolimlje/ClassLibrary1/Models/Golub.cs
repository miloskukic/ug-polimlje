﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Golub
    {
        private string id;
        private int idVlasnika;
        private int idVrste;
        private string pol;
        private string opis;

        public Golub(string id, int idVlasnika, int idVrste, string pol, string opis)
        {
            this.id = id;
            this.idVlasnika = idVlasnika;
            this.idVrste = idVrste;
            this.pol = pol;
            this.opis = opis;
        }

        public string Id { get => id; set => id = value; }
        public int IdVlasnika { get => idVlasnika; set => idVlasnika = value; }
        public int IdVrste { get => idVrste; set => idVrste = value; }
        public string Pol { get => pol; set => pol = value; }
        public string Opis { get => opis; set => opis = value; }
    }
}
