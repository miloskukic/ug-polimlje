﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Zapisnik
    {
        private String datumUtakmice;
        private int redniBrojUtakmice;
        private string oblacnost;
        private string pravacIJacinaVetra;
        private int vazdusniPritisak;
        private int temperatura;
        private string primedbeTakmicara;
        private string primedbePrvogSudije;
        private string primedbeDrugogSudije;

        public Zapisnik(String datumUtakmice, int redniBrojUtakmice, string oblacnost, string pravacIJacinaVetra, int vazdusniPritisak, int temperatura, string primedbeTakmicara, string primedbePrvogSudije, string primedbeDrugogSudije)
        {
            this.datumUtakmice = datumUtakmice;
            this.redniBrojUtakmice = redniBrojUtakmice;
            this.oblacnost = oblacnost;
            this.pravacIJacinaVetra = pravacIJacinaVetra;
            this.vazdusniPritisak = vazdusniPritisak;
            this.temperatura = temperatura;
            this.primedbeTakmicara = primedbeTakmicara;
            this.primedbePrvogSudije = primedbePrvogSudije;
            this.primedbeDrugogSudije = primedbeDrugogSudije;
        }

        public String DatumUtakmice { get => datumUtakmice; set => datumUtakmice = value; }
        public int RedniBrojUtakmice { get => redniBrojUtakmice; set => redniBrojUtakmice = value; }
        public string Oblacnost { get => oblacnost; set => oblacnost = value; }
        public string PravacIJacinaVetra { get => pravacIJacinaVetra; set => pravacIJacinaVetra = value; }
        public int VazdusniPritisak { get => vazdusniPritisak; set => vazdusniPritisak = value; }
        public int Temperatura { get => temperatura; set => temperatura = value; }
        public string PrimedbeTakmicara { get => primedbeTakmicara; set => primedbeTakmicara = value; }
        public string PrimedbePrvogSudije { get => primedbePrvogSudije; set => primedbePrvogSudije = value; }
        public string PrimedbeDrugogSudije { get => primedbeDrugogSudije; set => primedbeDrugogSudije = value; }
    }
}
