﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class PrijavaUtakmice
    {
        private int redniBroj;
        private String datumOdrzavanja;
        private int idKategorijeTakmicenja;

        public PrijavaUtakmice(int redniBroj, String datumOdrzavanja, int idKategorijeTakmicenja)
        {
            this.redniBroj = redniBroj;
            this.datumOdrzavanja = datumOdrzavanja;
            this.idKategorijeTakmicenja = idKategorijeTakmicenja;
        }

     



        public int RedniBroj { get => redniBroj; set => redniBroj = value; }
        public String DatumOdrzavanja { get => datumOdrzavanja; set => datumOdrzavanja = value; }
        public int IdKategorijeTakmicenja { get => idKategorijeTakmicenja; set => idKategorijeTakmicenja = value; }
    }
}
