﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Opis
    {
        private string vreme;
        private int redniBrojUtakmice;
        private string datumUtakmice;
        private string dodatniOpis;

        public Opis(int redniBrojUtakmice, string datumUtakmice, string sat, string dodatniOpis)
        {
            this.vreme = sat;
            this.redniBrojUtakmice = redniBrojUtakmice;
            this.datumUtakmice = datumUtakmice;
            this.dodatniOpis = dodatniOpis;
        }

        public string Vreme { get => vreme; set => vreme = value; }
        public int RedniBrojUtakmice { get => redniBrojUtakmice; set => redniBrojUtakmice = value; }
        public string DatumUtakmice { get => datumUtakmice; set => datumUtakmice = value; }
        public string DodatniOpis { get => dodatniOpis; set => dodatniOpis = value; }
    }
}
