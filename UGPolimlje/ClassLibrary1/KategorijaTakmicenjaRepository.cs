﻿using DataLayer.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class KategorijaTakmicenjaRepository
    {
        static private string MySqlConnectionstring = ConfigurationManager.ConnectionStrings["connstring"].ConnectionString;
        static private MySqlConnection databaseConnection = new MySqlConnection(MySqlConnectionstring);

        public DataTable Selektovanje(string kolone, string uslov)
        {
            //Pravljenje tabele kojoj ce biti prosledjene vrednosti iz baze
            DataTable dt = new DataTable();

            try
            {
                //Sql komanda za uzimanje podataka iz baze
                string sql = "SELECT " + kolone + " FROM kategorije " + uslov;
                //Izvrsavanje Sql komande
                MySqlCommand command = new MySqlCommand(sql, databaseConnection);
                //Popunjavanje tabele podacima
                MySqlDataAdapter adapter = new MySqlDataAdapter(command);
                databaseConnection.Open();
                adapter.Fill(dt);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                databaseConnection.Close();
            }
            return dt;
        }

        public bool UnosKategorijeTakmicenja(KategorijaTakmicenja kategorijaTakmicenja)
        {

            bool isSucces = false;

            try
            {
                //Sql komanda za unos podataka u baze
                string sql = "INSERT INTO kategorije VALUES (@id,@naziv)";

                //Izvrsavanje Sql komande
                MySqlCommand cmd = new MySqlCommand(sql, databaseConnection);
                cmd.Parameters.AddWithValue("@id", kategorijaTakmicenja.Id);
                cmd.Parameters.AddWithValue("@naziv", kategorijaTakmicenja.Naziv);

                databaseConnection.Open();
                int rows = cmd.ExecuteNonQuery();
                // Ako se izvrsi uspesno vrednost  rows  > 0
                if (rows > 0)
                {
                    isSucces = true;
                }
                else
                {
                    isSucces = false;
                }

            }

            catch (Exception ex)
            {
            }

            finally
            {
                databaseConnection.Close();
            }
            return isSucces;
        }

        //Metoda za azuriranje podataka u bazi
        public bool AzuriranjeKategorijeTakmicenja(KategorijaTakmicenja kategorijaTakmicenja)
        {
            bool isSucces = false;

            try
            {
                string sql = "UPDATE kategorije SET Vrsta=@naziv WHERE Id=@id";

                //Izvrsavanje Sql komande
                MySqlCommand cmd = new MySqlCommand(sql, databaseConnection);
                cmd.Parameters.AddWithValue("@id", kategorijaTakmicenja.Id);
                cmd.Parameters.AddWithValue("@naziv", kategorijaTakmicenja.Naziv);

                databaseConnection.Open();
                int rows = cmd.ExecuteNonQuery();
                // Ako se izvrsi uspesno vrednost  rows  > 0
                if (rows > 0)
                {
                    isSucces = true;
                }
                else
                {
                    isSucces = false;
                }

            }

            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("" + ex);
            }

            finally
            {
                databaseConnection.Close();
            }
            return isSucces;
        }

        //Metoda za brisanje podataka u bazi
        public bool BrisanjeKategorijeTakmicenja(int idKategorijeTakmicenja)
        {
            bool isSucces = false;

            try
            {
                //Sql komanda za brisanje podataka u baze
                string sql = "DELETE FROM kategorije WHERE Id=@Id";

                //Izvrsavanje Sql komande
                MySqlCommand cmd = new MySqlCommand(sql, databaseConnection);

                cmd.Parameters.AddWithValue("@Id", idKategorijeTakmicenja);

                databaseConnection.Open();
                int rows = cmd.ExecuteNonQuery();
                // Ako se izvrsi uspesno vrednost  rows  > 0
                if (rows > 0)
                {
                    isSucces = true;
                }
                else
                {
                    isSucces = false;
                }

            }

            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            finally
            {
                databaseConnection.Close();
            }
            return isSucces;

        }
    }
}
