﻿using DataLayer.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class ClanRepository
    {
        static private string MySqlConnectionstring = ConfigurationManager.ConnectionStrings["connstring"].ConnectionString;
        static private MySqlConnection databaseConnection = new MySqlConnection(MySqlConnectionstring);

        public DataTable Selektovanje(string kolone, string uslov)
        {
            //Pravljenje tabele kojoj ce biti prosledjene vrednosti iz baze
            DataTable dt = new DataTable();

            try
            {
                //Sql komanda za uzimanje podataka iz baze
                string sql = "SELECT " + kolone + " FROM clanovi " + uslov;
                //Izvrsavanje Sql komande
                MySqlCommand command = new MySqlCommand(sql, databaseConnection);
                //Popunjavanje tabele podacima
                MySqlDataAdapter adapter = new MySqlDataAdapter(command);
                databaseConnection.Open();
                adapter.Fill(dt);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                databaseConnection.Close();
            }
            return dt;
        }

        public bool UnosClana(Clan clan)
        {

            bool isSucces = false;

            try
            {
                //Sql komanda za unos podataka u baze
                string sql = "INSERT INTO clanovi VALUES (@id,@ime,@prezime,@jmbg,@brojLicneKarte,@adresa," +
                    "@datumRodjenja,@telefon,@telefon2,@email,@brojSL,@pol,@datum_uclanjenja)";
                
                //Izvrsavanje Sql komande
                MySqlCommand cmd = new MySqlCommand(sql, databaseConnection);
                cmd.Parameters.AddWithValue("@id", clan.Id);
                cmd.Parameters.AddWithValue("@ime", clan.Ime);
                cmd.Parameters.AddWithValue("@prezime", clan.Prezime);
                cmd.Parameters.AddWithValue("@jmbg", clan.Jmbg);
                cmd.Parameters.AddWithValue("@brojLicneKarte", clan.BrojLicneKarte);
                cmd.Parameters.AddWithValue("@adresa", clan.Adresa);
                cmd.Parameters.AddWithValue("@datumRodjenja", clan.DatumRodjenja);
                cmd.Parameters.AddWithValue("@telefon", clan.Telefon);
                cmd.Parameters.AddWithValue("@telefon2", clan.Telefon2);
                cmd.Parameters.AddWithValue("@email", clan.Email);
                cmd.Parameters.AddWithValue("@brojSL", clan.BrojSudijskeLegitimacije);
                cmd.Parameters.AddWithValue("@pol", clan.Pol);
                cmd.Parameters.AddWithValue("@datum_uclanjenja", clan.DatumUclanjenja);

                databaseConnection.Open();
                int rows = cmd.ExecuteNonQuery();
                // Ako se izvrsi uspesno vrednost  rows  > 0
                if (rows > 0)
                {
                    isSucces = true;
                }
                else
                {
                    isSucces = false;
                }

            }

            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            finally
            {
                databaseConnection.Close();
            }
            return isSucces;
        }

        //Metoda za azuriranje podataka u bazi
        public bool AzuriranjeClana(Clan clan)
        {
            bool isSucces = false;

            try
            {
                string sql = "UPDATE clanovi SET ime=@ime,prezime=@prezime,jmbg=@jmbg,brojLicneKarte=@brojLicneKarte,adresa=@adresa," +
                    "datumRodjenja=@datumRodjenja,telefon=@telefon,telefon2=@telefon2,email=@email,brojSL=@brojSL,pol=@pol,datum_uclanjenja=@datum_uclanjenja WHERE id=@id";
             
                //Izvrsavanje Sql komande
                MySqlCommand cmd = new MySqlCommand(sql, databaseConnection);
                cmd.Parameters.AddWithValue("@id", clan.Id);
                cmd.Parameters.AddWithValue("@ime", clan.Ime);
                cmd.Parameters.AddWithValue("@prezime", clan.Prezime);
                cmd.Parameters.AddWithValue("@jmbg", clan.Jmbg);
                cmd.Parameters.AddWithValue("@brojLicneKarte", clan.BrojLicneKarte);
                cmd.Parameters.AddWithValue("@adresa", clan.Adresa);
                cmd.Parameters.AddWithValue("@datumRodjenja", clan.DatumRodjenja);
                cmd.Parameters.AddWithValue("@telefon", clan.Telefon);
                cmd.Parameters.AddWithValue("@telefon2", clan.Telefon2);
                cmd.Parameters.AddWithValue("@email", clan.Email);
                cmd.Parameters.AddWithValue("@brojSL", clan.BrojSudijskeLegitimacije);
                cmd.Parameters.AddWithValue("@pol", clan.Pol);
                cmd.Parameters.AddWithValue("@datum_uclanjenja", clan.DatumUclanjenja);

                databaseConnection.Open();
                int rows = cmd.ExecuteNonQuery();
                // Ako se izvrsi uspesno vrednost  rows  > 0
                if (rows > 0)
                {
                    isSucces = true;
                }
                else
                {
                    isSucces = false;
                }

            }

            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("" + ex);
            }

            finally
            {
                databaseConnection.Close();
            }
            return isSucces;
        }

        //Metoda za brisanje podataka u bazi
        public bool BrisanjeClana(int idClana)
        {
            bool isSucces = false;

            try
            {
                //Sql komanda za brisanje podataka u baze
                string sql = "DELETE FROM clanovi WHERE id=@id";

                //Izvrsavanje Sql komande
                MySqlCommand cmd = new MySqlCommand(sql, databaseConnection);

                cmd.Parameters.AddWithValue("@id", idClana);

                databaseConnection.Open();
                int rows = cmd.ExecuteNonQuery();
                // Ako se izvrsi uspesno vrednost  rows  > 0
                if (rows > 0)
                {
                    isSucces = true;
                }
                else
                {
                    isSucces = false;
                }

            }

            catch (Exception ex)
            {
            }

            finally
            {
                databaseConnection.Close();
            }
            return isSucces;

        }
    }
}
