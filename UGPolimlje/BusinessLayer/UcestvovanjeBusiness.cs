﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class UcestvovanjeBusiness
    {
        UcestvovanjeRepository ucestvovanjeRepository;

        public UcestvovanjeBusiness()
        {
            this.ucestvovanjeRepository = new UcestvovanjeRepository();
        }

        public DataTable SelektovanjeSvihUcestvovanja()
        {
            return ucestvovanjeRepository.Selektovanje("*", "");
        }

        public bool UnosUcestvovanja(Ucestvovanje ucestvovanje)
        {
            return ucestvovanjeRepository.UnosUcestvovanja(ucestvovanje);
        }

        public bool BrisanjeUcestvovanja(int idClana, int redniBrojUtakmice, string datumOdrzavanja)
        {
            return ucestvovanjeRepository.BrisanjeUcestvovanja(idClana, redniBrojUtakmice, datumOdrzavanja);
        }
    }
}
