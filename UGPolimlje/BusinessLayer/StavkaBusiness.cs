﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class StavkaBusiness
    {
        StavkaRepository stavkaRepository;

        public StavkaBusiness()
        {
            this.stavkaRepository = new StavkaRepository();
        }

        public DataTable SelektovanjeSvihStavki()
        {
            return stavkaRepository.Selektovanje("*", "");
        }


        public DataTable Selektovanje(string kolone, string uslov)
        {
            return stavkaRepository.Selektovanje(kolone, uslov);
        }


        public bool UnosStavke(Stavka stavka)
        {
            return stavkaRepository.UnosStavke(stavka);
        }

        public bool AzuriranjeStavke(Stavka stavka)
        {
            return stavkaRepository.AzuriranjeStavke(stavka);
        }

        public bool BrisanjeStavke(string datumOdrzavanja, int redniBrojUtakmice, int idGoluba)
        {
            return stavkaRepository.BrisanjeStavke(datumOdrzavanja, redniBrojUtakmice, idGoluba);
        }
    }
}
