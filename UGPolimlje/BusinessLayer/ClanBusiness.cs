﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class ClanBusiness
    {
        ClanRepository clanRepository;

        public ClanBusiness()
        {
            this.clanRepository = new ClanRepository();
        }

        public DataTable SelektovanjeSvihClanova()
        {
            return clanRepository.Selektovanje("*", "");
        }

        public DataTable Selektovanje(string kolona,string uslov)
        {
            return clanRepository.Selektovanje(kolona, uslov);
        }

        public bool UnosClana(Clan clan)
        {
            return clanRepository.UnosClana(clan);
        }

        public bool AzuriranjeClana(Clan clan)
        {
            return clanRepository.AzuriranjeClana(clan);
        }

        public bool BrisanjeClana(int idClana)
        {
            return clanRepository.BrisanjeClana(idClana);
        }
    }
}
