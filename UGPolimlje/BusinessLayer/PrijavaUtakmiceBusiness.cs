﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class PrijavaUtakmiceBusiness
    {
        PrijavaUtakmiceRepository prijavaUtakmiceRepository;

        public PrijavaUtakmiceBusiness()
        {
            this.prijavaUtakmiceRepository = new PrijavaUtakmiceRepository();
        }

        public DataTable SelektovanjeSvihPrijavaUtakmica()
        {
            return prijavaUtakmiceRepository.Selektovanje("*", "");
        }

        public bool UnosPrijaveUtakmice(PrijavaUtakmice prijavaUtakmice)
        {
            return prijavaUtakmiceRepository.UnosPrijaveUtakmice(prijavaUtakmice);
        }

        public bool BrisanjePrijaveUtakmice(int redniBrojUtakmice, string datumOdrzavanja)
        {
            return prijavaUtakmiceRepository.BrisanjePrijaveUtakmice(redniBrojUtakmice, datumOdrzavanja);
        }
    }
}
