﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class OpisBusiness
    {
        OpisRepository opisRepository;

        public OpisBusiness()
        {
            this.opisRepository = new OpisRepository();
        }

        public DataTable SelektovanjeSvihOpisa()
        {
            return opisRepository.Selektovanje("*", "");
        }

        public DataTable Selektovanje(string kolone,string uslov)
        {
            return opisRepository.Selektovanje(kolone, uslov);
        }

        public bool UnosOpisa(Opis opis)
        {
            return opisRepository.UnosOpisa(opis);
        }

        public bool AzuriranjeOpisa(Opis opis)
        {
            return opisRepository.AzuriranjeOpisa(opis);
        }

        public bool BrisanjeOpisa(string vreme, int redniBrojUtakmice, string datumOdrzavanja)
        {
            return opisRepository.BrisanjeOpisa(vreme, redniBrojUtakmice, datumOdrzavanja);
        }
    }
}
