﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class ZapisnikBusiness
    {
        ZapisnikRepository zapisnikRepository;

        public ZapisnikBusiness()
        {
            this.zapisnikRepository = new ZapisnikRepository();
        }

        public DataTable SelektovanjeSvihZapisnika()
        {
            return zapisnikRepository.Selektovanje("*", "");
        }

        public DataTable Selektovanje(string id, string uslov)
        {
            return zapisnikRepository.Selektovanje(id, uslov);
        }

        public bool UnosZapisnika(Zapisnik zapisnik)
        {
            return zapisnikRepository.UnosZapisnika(zapisnik);
        }

        public bool AzuriranjeZapisnika(Zapisnik zapisnik)
        {
            return zapisnikRepository.AzuriranjeZapisnika(zapisnik);
        }

        public bool BrisanjeZapisnika(string datumOdrzavanja, int redniBrojUtakmice)
        {
            return zapisnikRepository.BrisanjeZapisnika(datumOdrzavanja, redniBrojUtakmice);
        }
    }
}
