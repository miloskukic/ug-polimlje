﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class KategorijaTakmicenjaBusiness
    {
        KategorijaTakmicenjaRepository kategorijaTakmicenjaRepository;

        public KategorijaTakmicenjaBusiness()
        {
            this.kategorijaTakmicenjaRepository = new KategorijaTakmicenjaRepository();
        }

        public DataTable Selektovanje(string id, string uslov)
        {
            return kategorijaTakmicenjaRepository.Selektovanje(id, uslov);
        }

        public DataTable SelektovanjeSvihKategorijaTakmicenja()
        {
            return kategorijaTakmicenjaRepository.Selektovanje("*", "");
        }

        public bool UnosKategorijeTakmicenja(KategorijaTakmicenja kategorijaTakmicenja)
        {
            return kategorijaTakmicenjaRepository.UnosKategorijeTakmicenja(kategorijaTakmicenja);
        }

        public bool AzuriranjeKategorijeTakmicenja(KategorijaTakmicenja kategorijaTakmicenja)
        {
            return kategorijaTakmicenjaRepository.AzuriranjeKategorijeTakmicenja(kategorijaTakmicenja);
        }

        public bool BrisanjeKategorijeTakmicenja(int idKategorijeTakmicenja)
        {
            return kategorijaTakmicenjaRepository.BrisanjeKategorijeTakmicenja(idKategorijeTakmicenja);
        }
    }
}
