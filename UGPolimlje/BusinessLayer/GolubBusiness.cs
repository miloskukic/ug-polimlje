﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class GolubBusiness
    {
        GolubRepository golubRepository;

        public GolubBusiness()
        {
            this.golubRepository = new GolubRepository();
        }

        public DataTable SelektovanjeSvihGolubova()
        {
            return golubRepository.Selektovanje("*", "");
        }

        public DataTable Selektovanje(string kolone,string uslov)
        {
            return golubRepository.Selektovanje(kolone, uslov);
        }


        public bool UnosGoluba(Golub golub)
        {
            return golubRepository.UnosGoluba(golub);
        }

        public bool AzuriranjeGoluba(Golub golub)
        {
            return golubRepository.AzuriranjeGoluba(golub);
        }

        public bool BrisanjeGoluba(string idGoluba)
        {
            return golubRepository.BrisanjeGoluba(idGoluba);
        }
    }
}
