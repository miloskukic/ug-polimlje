﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class VrstaGolubaBusiness
    {
        VrstaGolubaRepository vrstaGolubaRepository;

        public VrstaGolubaBusiness()
        {
            this.vrstaGolubaRepository = new VrstaGolubaRepository();
        }

        public DataTable Selektovanje(string kolona,string uslov)
        {
            return vrstaGolubaRepository.Selektovanje(kolona, uslov);
        }

        public DataTable SelektovanjeSvihVrstiGolubova()
        {
            return vrstaGolubaRepository.Selektovanje("*", "");
        }

        public bool UnosVrsteGoluba(VrstaGoluba vrstaGoluba)
        {
            return vrstaGolubaRepository.UnosVrsteGoluba(vrstaGoluba);
        }

        public bool AzuriranjeVrsteGoluba(VrstaGoluba vrstaGoluba)
        {
            return vrstaGolubaRepository.AzuriranjeVrsteGoluba(vrstaGoluba);
        }

        public bool BrisanjeVrsteGoluba(int idVrste)
        {
            return vrstaGolubaRepository.BrisanjeVrsteGoluba(idVrste);
        }
    }
}
