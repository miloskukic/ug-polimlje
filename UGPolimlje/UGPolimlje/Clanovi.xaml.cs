﻿using System;
using DataLayer.Models;
using System.Windows;
using BusinessLayer;
using System.Data;
using System.Windows.Controls;

namespace UGPolimlje
{
    /// <summary>
    /// Interaction logic for Clanovi.xaml
    /// </summary>
    public partial class Clanovi : Window
    {
        private ClanBusiness clanoviBussines = new ClanBusiness();
        private bool izmenaL = false;
        bool success = false;
        bool exceptionS = true;

        public Clanovi(int izmena)
        {
            InitializeComponent();
            if (izmena != -1)
            {
                String pol = "";
                this.Title = "Clanovi [Izmena postojeceg]";
                foreach (DataRow row in clanoviBussines.Selektovanje("*", " WHERE id=" + izmena + "").Rows)
                {
                    TextBoxId.Text = row[0].ToString();
                    TextBoxIme.Text = row[1].ToString();
                    TextBoxPrezime.Text = row[2].ToString();
                    TextBoxJmbg.Text = row[3].ToString();
                    TextBoxBrLicne.Text = row[4].ToString();
                    TextBoxAdresa.Text = row[5].ToString();
                    DatePickDatumRodjenja.SelectedDate = Convert.ToDateTime(row[6].ToString());
                    TextBoxTelefon.Text = row[7].ToString(); ;
                    TextBoxTelefon1.Text = row[8].ToString(); ;
                    TextBoxEmail.Text = row[9].ToString();
                    TextBoxBrSL.Text = row[10].ToString();
                    pol = row[11].ToString();
                    DatePickUclanjenja.SelectedDate = Convert.ToDateTime(row[12].ToString());
                    if (pol.Equals("muski")) {
                        ComboPol.SelectedItem = "muski";
                    }
                    else
                    {
                        ComboPol.SelectedItem = "zenski";
                    }

                    //DatePickUclanjenja.SelectedDate = Convert.ToDateTime(row[12].ToString());


                    //brojSL = ;


                }
                izmenaL = true;
            }
            else
            {
                this.Title = "Clanovi [Dodavanje novog]";
            }


        }

        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        private void BtnPotvrdi_Click(object sender, RoutedEventArgs e)
        {
            int id = 0;
            String jmbg = "", brLCK = "", ime = "", prezime = "",
            telefon = "", telefon2 = "", email = "", datumRodjenja = "", adresa = "",
            datumUclanjenja = "", brojSL = "", pol = "";

            try
            {
                id = Convert.ToInt32(TextBoxId.Text);
            }
            catch (FormatException)
            {
                exceptionS = false;
            }


            jmbg = TextBoxJmbg.Text;
            brLCK = TextBoxBrLicne.Text;
            ime = TextBoxIme.Text;
            prezime = TextBoxPrezime.Text;
            telefon = TextBoxTelefon.Text;
            telefon2 = TextBoxTelefon1.Text;
            email = TextBoxEmail.Text;
            try
            {
                DateTime datumRodjenjaD = DatePickDatumRodjenja.SelectedDate.Value;
                datumRodjenja = datumRodjenjaD.ToString("yyyy-MM-dd");
                adresa = TextBoxAdresa.Text;
                DateTime datumUclanjenjaD = DatePickUclanjenja.SelectedDate.Value;
                datumUclanjenja = datumUclanjenjaD.ToString("yyyy-MM-dd");
            }
            catch (Exception ex)
            {
                exceptionS = false;
            }
            brojSL = TextBoxBrSL.Text;
            pol = ComboPol.SelectedItem.ToString();

            if (ime.Equals("") && prezime.Equals(""))
            {
                exceptionS = false;
            }
            
            Clan clanovi = new Clan(id, ime, prezime, jmbg, brLCK, adresa, datumRodjenja, telefon, telefon2, email, brojSL, pol, datumUclanjenja);

            if (exceptionS)
            {
                if (izmenaL == true)
                {
                    success = clanoviBussines.AzuriranjeClana(clanovi);
                }
                else
                {
                    success = clanoviBussines.UnosClana(clanovi);
                }
            }
           
            this.Close();

            if (success)
            {
                if (izmenaL)
                {
                    MessageBox.Show("Uspesno editovanju zapisa", "Clanovi", MessageBoxButton.OK, MessageBoxImage.Information);

                }
                else
                {
                    MessageBox.Show("Uspesno dodavanje novog zapisa", "Clanovi", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            else
            {
                if (izmenaL)
                {
                    MessageBox.Show("Greska u editovanju zapisa, proverite sintaksu i moguce dupliranje zapisa, obavezna polja (*) ne smeju biti prazna.", "Clanovi", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    MessageBox.Show("Greska u dodavanju novog zapisa, proverite sintaksu i moguce dupliranje zapisa, obavezna polja (*) ne smeju biti prazna.", "Clanovi", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void ComboPol_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            combo.Items.Add("muski");
            combo.Items.Add("zenski");
            combo.SelectedIndex = 0;
        }
    }
}
