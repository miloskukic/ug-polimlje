﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UGPolimlje
{
    /// <summary>
    /// Interaction logic for Golubovi.xaml
    /// </summary>
    public partial class Kategorije : Window
    {
        private KategorijaTakmicenjaBusiness kategorijaTakmicenjaBusiness;
        private bool izmenaL = false;
        bool success = false;
        bool exceptionS = true;

        public Kategorije(int izmena)
        {
            InitializeComponent();
            kategorijaTakmicenjaBusiness = new KategorijaTakmicenjaBusiness();
            if (izmena != -1)
            {
                this.Title = "Kategorije [Izmena postojece]";
                foreach (DataRow row in kategorijaTakmicenjaBusiness.Selektovanje("*", " WHERE id=" + izmena + "").Rows)
                {
                    TextBoxId.Text = row[0].ToString();
                    TextBoxNaziv.Text = row[1].ToString();
                }
                TextBoxId.IsEnabled = false;
                izmenaL = true;
            }
            else
            {
                this.Title = "Kategorije [Dodavanje nove]";
            }
        }

        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnPotvrdi_Click(object sender, RoutedEventArgs e)
        {
            int id=0; 
            string naziv="";
            try
            {
                id = Convert.ToInt32(TextBoxId.Text);
            }
            catch(FormatException fe)
            {
                exceptionS = false;
            }
           
            naziv = TextBoxNaziv.Text;

            if (naziv.Equals(""))
            {
                exceptionS = false;
            }

            KategorijaTakmicenja kt = new KategorijaTakmicenja(id, naziv);

            if (exceptionS)
            {
                if (izmenaL == true)
                {
                    success = kategorijaTakmicenjaBusiness.AzuriranjeKategorijeTakmicenja(kt);
                }
                else
                {
                    success = kategorijaTakmicenjaBusiness.UnosKategorijeTakmicenja(kt);
                }
            }
           
            //Tabele tabeleWindow = new Tabele();
            //DataTable dt = kategorijaTakmicenjaBusiness.SelektovanjeSvihKategorijaTakmicenja();
            //tabeleWindow.DataGridView.DataContext = dt.DefaultView;
            this.Close();

            if (success)
            {
                if (izmenaL)
                {
                    MessageBox.Show("Uspesno editovanju zapisa", "Kategorija", MessageBoxButton.OK, MessageBoxImage.Information);

                }
                else
                {
                    MessageBox.Show("Uspesno dodavanje novog zapisa", "Kategorija", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            else
            {
                if (izmenaL)
                {
                    MessageBox.Show("Greska u editovanju zapisa, proverite sintaksu i moguce dupliranje zapisa, obavezna polja (*) ne smeju biti prazna.", "Kategorija", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    MessageBox.Show("Greska u dodavanju novog zapisa, proverite sintaksu i moguce dupliranje zapisa, obavezna polja (*) ne smeju biti prazna.", "Kategorija", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
    }
}
