﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UGPolimlje
{
    /// <summary>
    /// Interaction logic for Prijava.xaml
    /// </summary>
    public partial class Prijava : Window
    {
        public Prijava()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string name, pass;
            name = NameTextBox.Text.ToString();
            pass = PassTextBox.Password.ToString();
            MessageBox.Show("Vas username je: " + name + " Vasa lozinka je: " + pass, "Prijavili ste se kao", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            Pocetna pocetna = new Pocetna();
            pocetna.Show();
            this.Close();
        }

    }
}
