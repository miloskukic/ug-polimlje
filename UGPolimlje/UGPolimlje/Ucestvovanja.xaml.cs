﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UGPolimlje
{
    /// <summary>
    /// Interaction logic for Opisi.xaml
    /// </summary>
    public partial class Ucestvovanja : Window
    {
        private UcestvovanjeBusiness ucestvovanjeBusiness = new UcestvovanjeBusiness();
        private ClanBusiness clanBusiness = new ClanBusiness();
        private PrijavaUtakmiceBusiness prijavaUtakmiceBusiness = new PrijavaUtakmiceBusiness();
        bool success = false;
        bool exceptionS = true;

        public Ucestvovanja(int izmena)
        {
            InitializeComponent();
            switch (izmena)
            {
                case 0: this.Title = "Ucestvovanje [Dodavanje novog]"; break;
                case 1: this.Title = "Ucestvovanje [Izmena postojeceg]"; break;
            }
        }

        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnPotvrdi_Click(object sender, RoutedEventArgs e)
        {
            int id = 0, idUloge = 0, rbrUtakmice = 0;
            String datumUtakmice = "", uloga = "", napomena = "";

            string[] clan = ComboClan.SelectedItem.ToString().Split(' ');

            foreach (DataRow row in clanBusiness.Selektovanje("id", "WHERE id=\"" + clan[0] + "\" and ime=\"" + clan[1] + "\" and prezime=\"" + clan[2] + "\"").Rows)
            {
                id = Convert.ToInt32(row[0]);
            }

            try
            {
                rbrUtakmice = Convert.ToInt32(ComboRbrUtakmice.SelectedItem.ToString());
                datumUtakmice = ComboDatumUtakmice.SelectedItem.ToString();
            }
            catch(Exception ex)
            {
                datumUtakmice = "1970-01-01";
                exceptionS = false;
            } 
            uloga = ComboUloga.SelectedItem.ToString();
            napomena = TextBoxNapomena.Text;
            Ucestvovanje u = new Ucestvovanje(id, datumUtakmice, rbrUtakmice, uloga, napomena);

            if (exceptionS)
            {
                success = ucestvovanjeBusiness.UnosUcestvovanja(u);
            }

            this.Close();

          
            if (success)
            {
                MessageBox.Show("Uspesno dodavanje novog zapisa", "Ucestovanja", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("Greska u dodavanju novog zapisa, proverite sintaksu i moguce dupliranje zapisa", "Ucestvovanja", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ComboClan_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            foreach (DataRow row in clanBusiness.SelektovanjeSvihClanova().Rows)
            {
                combo.Items.Add(row[0].ToString() + " " + row[1].ToString() + " " + row[2].ToString());
            }
            combo.SelectedIndex = 0;
        }

        private void ComboRbrUtakmice_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            foreach (DataRow row in prijavaUtakmiceBusiness.SelektovanjeSvihPrijavaUtakmica().Rows)
            {
                combo.Items.Add(row[0].ToString());
            }
            combo.SelectedIndex = 0;
        }

        private void ComboDatumUtakmice_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            foreach (DataRow row in prijavaUtakmiceBusiness.SelektovanjeSvihPrijavaUtakmica().Rows)
            {
                DateTime datumUtakmiceD = Convert.ToDateTime(row[1]);
                string datumUtakmice = datumUtakmiceD.ToString("yyyy-MM-dd");
                combo.Items.Add(datumUtakmice);
            }
            combo.SelectedIndex = 0;
        }

        private void ComboUloga_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            combo.Items.Add("takmicar");
            combo.Items.Add("sudija");
            combo.SelectedIndex = 0;
        }
    }
}
