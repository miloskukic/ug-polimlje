﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;

namespace UGPolimlje
{
    /// <summary>
    /// Interaction logic for Opisi.xaml
    /// </summary>
    public partial class PrijaveUtakmica : Window
    {
        private PrijavaUtakmiceBusiness utakmiceBusiness = new PrijavaUtakmiceBusiness();
        private KategorijaTakmicenjaBusiness kategorijaTakmicenjaBusiness = new KategorijaTakmicenjaBusiness();

        bool success = false;
        bool exceptionS = true;

        public PrijaveUtakmica(int izmena)
        {
            InitializeComponent();
            switch (izmena)
            {
                case 0: this.Title = "Prijava utakmice [Dodavanje novog]"; break;
                case 1: this.Title = "Prijava utakmice [Izmena postojeceg]"; break;
            }
        }




        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            
        }

        private void BtnPotvrdi_Click(object sender, RoutedEventArgs e)
        {
            int id = 0, kategorija = 0;
            String datum2 = "";
            try
            {
                id = Convert.ToInt32(TextBoxRbr.Text);
                DateTime datum = DatePickUtakmice.SelectedDate.Value;
                datum2 = datum.ToString("yyyy-MM-dd");


            }
            catch(Exception ex)
            {
               exceptionS = false;
            }

            foreach (DataRow row in kategorijaTakmicenjaBusiness.Selektovanje("id"," WHERE Vrsta=\""+ComboKategorija.SelectedItem.ToString()+"\"").Rows)
            {
                kategorija = Convert.ToInt32(row[0].ToString());
            }

            PrijavaUtakmice pu = new PrijavaUtakmice(id, datum2, kategorija);

            if (exceptionS)
            {
                success = utakmiceBusiness.UnosPrijaveUtakmice(pu);
            }

            this.Close();

         
            if (success)
            {
                MessageBox.Show("Uspesno dodavanje novog zapisa", "Prijave utakmica", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("Greska u dodavanju novog zapisa, proverite sintaksu i moguce dupliranje zapisa", "Prijava utakmica", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ComboKategorija_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            foreach (DataRow row in kategorijaTakmicenjaBusiness.SelektovanjeSvihKategorijaTakmicenja().Rows)
            {
                combo.Items.Add(row[1].ToString());
            }
            combo.SelectedIndex = 0;
        }
    }
}
