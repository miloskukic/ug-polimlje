﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UGPolimlje
{
    /// <summary>
    /// Interaction logic for Golubovi.xaml
    /// </summary>
    public partial class Golubovi : Window
    {
        GolubBusiness golubBusiness = new GolubBusiness();
        VrstaGolubaBusiness vrstaGolubaBusiness = new VrstaGolubaBusiness();
        ClanBusiness clanBusiness = new ClanBusiness();

        private bool izmenaL = false;
        private string datumL = "",polL="",golubL="";
        private int idL = 0,vrstaL=0;
        bool success = false;

        

        public Golubovi(int izmena,string idGoluba)
        {
            InitializeComponent();
            if (izmena != -1 || !(idGoluba.Equals("")))
            {
                this.Title = "Golub [Dodavanje novog]";
                foreach (DataRow row in golubBusiness.Selektovanje("*", " WHERE idVlasnika=" + izmena + " and Id = \"" + idGoluba + "\"").Rows)
                {
                    TextBoxGolubId.Text = row[0].ToString();
                    polL = row[2].ToString();
                    vrstaL = Convert.ToInt32(row[3].ToString());
                    TextBoxOpis.Text = row[4].ToString();

                }
                TextBoxGolubId.IsEnabled = false;
                idL = izmena;
                golubL = idGoluba;
                izmenaL = true;
            }
            else
            {
                TextBoxGolubId.IsEnabled = true;
                this.Title = "Golub [Izmena postojeceg]";
            }

            //ComboVrsta.ItemsSource = data;
        }

        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnPotvrdi_Click(object sender, RoutedEventArgs e)
        {
            int id = 0, polId=0,vrsteId=0;
            String idGoluba = "", vrste = "", pol = "", opis = "";
            //id = Convert.ToInt32(TextBoxIdVlasnika.Text);
            idGoluba = TextBoxGolubId.Text;
            pol = ComboPol1.SelectedItem.ToString();
             
            foreach (DataRow row in vrstaGolubaBusiness.Selektovanje("id","WHERE naziv=\""+ComboVrsta.SelectedItem.ToString()+"\"").Rows)
            {
                 vrsteId = Convert.ToInt32(row[0]);
            }

            string[] clan = ComboClan.SelectedItem.ToString().Split(' ');

            foreach (DataRow row in clanBusiness.Selektovanje("id", "WHERE id=\"" + clan[0] + "\" and ime=\""+clan[1]+"\" and prezime=\""+clan[2]+"\"").Rows)
            {
                id = Convert.ToInt32(row[0]);
            }


            opis = TextBoxOpis.Text;
            Golub g = new Golub(idGoluba, id,vrsteId,pol,opis);

            if (izmenaL == true)
            {
                success = golubBusiness.AzuriranjeGoluba(g);
            }
            else
            {
                success =  golubBusiness.UnosGoluba(g);
            }
          
            this.Close();

            if (success)
            {
                if (izmenaL)
                {
                    MessageBox.Show("Uspesno editovanju zapisa", "Golubovi", MessageBoxButton.OK, MessageBoxImage.Information);

                }
                else
                {
                    MessageBox.Show("Uspesno dodavanje novog zapisa", "Golubovi", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            else
            {
                if (izmenaL)
                {
                    MessageBox.Show("Greska u editovanju zapisa, proverite sintaksu i moguce dupliranje zapisa, obavezna pola (*) moraju biti popunjena", "Golubovi", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    MessageBox.Show("Greska u dodavanju novog zapisa, proverite sintaksu i moguce dupliranje zapisa, obavezna pola (*) moraju biti popunjena", "Golubovi", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void ComboVrsta_Loaded(object sender, RoutedEventArgs e)
        {
            
            var combo = sender as ComboBox;

            string trenutnaVrsta = "";
            int index = 0,br=0;

            foreach (DataRow row in vrstaGolubaBusiness.Selektovanje("naziv", "WHERE id=" + vrstaL + "").Rows)
            {
                trenutnaVrsta = row[0].ToString().ToLower();
            }

            foreach (DataRow row in vrstaGolubaBusiness.SelektovanjeSvihVrstiGolubova().Rows)
            {
                combo.Items.Add(row[1].ToString());
                if(izmenaL == true)
                {
                    if (row[1].ToString().ToLower().Equals(trenutnaVrsta))
                    {
                        index = br;
                    }
                    br++;
                }
            }

            if (izmenaL == true)
            {
                combo.SelectedIndex = index;
            }
            else
            {
                combo.SelectedIndex = 0;
            }
        }

        private void ComboClan_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            if (izmenaL == true)
            {
                foreach (DataRow row in clanBusiness.Selektovanje("*", " WHERE id=" + idL + "").Rows)
                {
                    combo.Items.Add(row[0].ToString() + " " + row[1].ToString() + " " + row[2].ToString());
                }
                combo.IsEnabled = false;
            }
            else
            {
                foreach (DataRow row in clanBusiness.SelektovanjeSvihClanova().Rows)
                {
                    combo.Items.Add(row[0].ToString() + " " + row[1].ToString() + " " + row[2].ToString());
                }
                combo.IsEnabled = true;
            }
           
            combo.SelectedIndex = 0;
        }

        private void ComboPol1_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            combo.Items.Add("muski");
            combo.Items.Add("zenski");
            if (izmenaL == true)
            {
                if (polL.ToLower().Equals("muski"))
                {
                    combo.SelectedIndex = 0;
                }
                else
                {
                    combo.SelectedIndex = 1;
                }
            }
            else
            {
                combo.SelectedIndex = 0;
            }
        }
    }
}
