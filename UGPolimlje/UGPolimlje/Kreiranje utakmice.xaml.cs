﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UGPolimlje
{
    /// <summary>
    /// Interaction logic for Kreiranje_utakmice.xaml
    /// </summary>
    public partial class Kreiranje_utakmice : Window
    {
        private ClanBusiness clanBusiness = new ClanBusiness();
        private PrijavaUtakmiceBusiness prijavaUtakmiceBusiness = new PrijavaUtakmiceBusiness();
        private KategorijaTakmicenjaBusiness kategorijaTakmicenjaBusiness = new KategorijaTakmicenjaBusiness();
        private UcestvovanjeBusiness ucestvovanjeBusiness = new UcestvovanjeBusiness();

        private bool success = false;
        private bool exceptionS = true, korakP = false, u1 = false, u2 = false, u3 = false;
        private int rbr = 0, idClana = 0, korak = 0, brUc = 0;
        private string datumUtakmice = "";

        public Kreiranje_utakmice()
        {
            InitializeComponent();
        }

        private void ComboKategorija_Copy_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            combo.Items.Add("Juniori");
            combo.Items.Add("Seniori");
            combo.SelectedIndex = 0;
        }

        private void ComboClan_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            foreach (DataRow row in clanBusiness.SelektovanjeSvihClanova().Rows)
            {
                combo.Items.Add(row[0].ToString() + " " + row[1].ToString() + " " + row[2].ToString());
            }
            combo.SelectedIndex = 0;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //Pocetna p = new Pocetna();
            //p.Show();
        }

        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            Pocetna p = new Pocetna();
            p.Show();
            this.Close();
        }

        private void ComboClan_Copy_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            foreach (DataRow row in clanBusiness.SelektovanjeSvihClanova().Rows)
            {
                combo.Items.Add(row[0].ToString() + " " + row[1].ToString() + " " + row[2].ToString());
            }
            combo.SelectedIndex = 0;
        }

        private void ComboClan_Copy1_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            foreach (DataRow row in clanBusiness.SelektovanjeSvihClanova().Rows)
            {
                combo.Items.Add(row[0].ToString() + " " + row[1].ToString() + " " + row[2].ToString());
            }
            combo.SelectedIndex = 0;
        }

        private void BtnPotvrdi_Click(object sender, RoutedEventArgs e)
        {
             kreirajUtakmicu();
             dodajUcestvovanje(ComboClanT.SelectedItem.ToString(), "takmicar");
             dodajUcestvovanje(ComboClanS1.SelectedItem.ToString(), "sudija");
             dodajUcestvovanje(ComboClanS2.SelectedItem.ToString(), "sudija");
             resetujPolja();
            //this.Close();

        }

        public void resetujPolja()
        {
            TextBoxRbr.Text = "";
            ComboKategorija.SelectedIndex = 0;
            ComboClanT.SelectedIndex = 0;
            ComboClanS1.SelectedIndex = 0;
            ComboClanS2.SelectedIndex = 0;
            DateTime datum = DateTime.Now;
            DatePickUtakmice.SelectedDate = datum;
        }

        private void dodajUcestvovanje(string clanIdImePrezime, string uloga)
        {

            string[] clan = clanIdImePrezime.Split(' ');

            foreach (DataRow row in clanBusiness.Selektovanje("id", "WHERE id=\"" + clan[0] + "\" and ime=\"" + clan[1] + "\" and prezime=\"" + clan[2] + "\"").Rows)
            {
                idClana = Convert.ToInt32(row[0]);
            }

            Ucestvovanje u = new Ucestvovanje(idClana, datumUtakmice, rbr, uloga, "Utakmica kreirana pomocu carobnjaka");
            success = ucestvovanjeBusiness.UnosUcestvovanja(u);
        }


        private void kreirajUtakmicu()
        {
            int kategorija = 0;

            try
            {
                rbr = Convert.ToInt32(TextBoxRbr.Text);
                DateTime datum = DatePickUtakmice.SelectedDate.Value;
                datumUtakmice = datum.ToString("yyyy-MM-dd");
            }
            catch (Exception ex)
            {
                exceptionS = false;
            }

            foreach (DataRow row in kategorijaTakmicenjaBusiness.Selektovanje("id", " WHERE Vrsta=\"" + ComboKategorija.SelectedItem.ToString() + "\"").Rows)
            {
                kategorija = Convert.ToInt32(row[0].ToString());
            }

            PrijavaUtakmice pu = new PrijavaUtakmice(rbr, datumUtakmice, kategorija);

            if (exceptionS)
            {
                success = prijavaUtakmiceBusiness.UnosPrijaveUtakmice(pu);
            }


        }

        private void ComboKategorija_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            foreach (DataRow row in kategorijaTakmicenjaBusiness.SelektovanjeSvihKategorijaTakmicenja().Rows)
            {
                combo.Items.Add(row[1].ToString());
            }
            combo.SelectedIndex = 0;
        }
    }
}
