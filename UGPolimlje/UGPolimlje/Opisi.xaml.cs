﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UGPolimlje
{
    /// <summary>
    /// Interaction logic for Opisi.xaml
    /// </summary>
    public partial class Opisi : Window
    {
        private PrijavaUtakmiceBusiness prijavaUtakmiceBusiness = new PrijavaUtakmiceBusiness();
        private OpisBusiness opisBussines = new OpisBusiness();

        private bool izmenaL = false;
        private string datumL = "";
        private int idL = 0, vS = 0, vM = 0;
        bool success = false;
      

        public Opisi(int izmena,string datum,string vreme)
        {
            InitializeComponent();
            if (izmena != -1 || !(datum.Equals("")) || !(vreme.Equals("")) )
            {
                this.Title = "Opis [Izmena postojeceg]";
                string vremeL = "";
                string[] vremeN;
                DateTime datumUtakmiceD = Convert.ToDateTime(datum);
                string datumUtakmiceP = datumUtakmiceD.ToString("yyyy-MM-dd");

                foreach (DataRow row in opisBussines.Selektovanje("*", " WHERE rbr_utakmice=" + izmena + " and datumUtakmice = \"" + datumUtakmiceP + "\"" + " and vreme = \"" + vreme + "\"").Rows)
                {
                    vremeL= row[0].ToString();
                    TextBoxOpis.Text = row[3].ToString();
                }

                vremeN = vremeL.Split(':');
                vS = Convert.ToInt32(vremeN[0]);
                vM = Convert.ToInt32(vremeN[1]);
                Console.WriteLine(vS + " : " + vM);
                izmenaL = true;
                datumL = datum;
                idL = izmena;
            }
            else
            {
                this.Title = "Opis [Dodavanje novog]";
            }
        }

        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnPotvrdi_Click(object sender, RoutedEventArgs e)
        {
            int rbr=0;
            String datumUtakmice = "", vreme = "", opis = "";
            rbr = Convert.ToInt32(ComboRbr.SelectedItem.ToString());
            datumUtakmice = ComboDatumUtakmice.SelectedItem.ToString();
            vreme = ComboSati.SelectedItem.ToString()+":"+ ComboMinuti.SelectedItem.ToString();
            opis = TextBoxOpis.Text;
            Opis o = new Opis(rbr, datumUtakmice, vreme, opis);

            if(izmenaL == true)
            {
                success =  opisBussines.AzuriranjeOpisa(o);
            }
            else
            {
                success =  opisBussines.UnosOpisa(o);
            }
            this.Close();

            if (success)
            {
                if (izmenaL)
                {
                    MessageBox.Show("Uspesno editovanju zapisa", "Opisi", MessageBoxButton.OK, MessageBoxImage.Information);

                }
                else
                {
                    MessageBox.Show("Uspesno dodavanje novog zapisa", "Opisi", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            else
            {
                if (izmenaL)
                {
                    MessageBox.Show("Greska u editovanju zapisa, proverite sintaksu i moguce dupliranje zapisa", "Opisi", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    MessageBox.Show("Greska u dodavanju novog zapisa, proverite sintaksu i moguce dupliranje zapisa", "Opisi", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void ComboRbr_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            if (izmenaL == true)
            {
                combo.Items.Add(idL);
                combo.IsEnabled = false;
            }
            else
            {
                foreach (DataRow row in prijavaUtakmiceBusiness.SelektovanjeSvihPrijavaUtakmica().Rows)
                {
                    combo.Items.Add(row[0].ToString());
                }
                combo.IsEnabled = true;
            }
            combo.SelectedIndex = 0;
        }

        private void ComboDatumUtakmice_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            if (izmenaL == true)
            {
                DateTime datumUtakmiceDL = Convert.ToDateTime(datumL);
                string datumUtakmiceL = datumUtakmiceDL.ToString("yyyy-MM-dd");
                combo.Items.Add(datumUtakmiceL);
                combo.IsEnabled = false;
            }
            else
            {
                foreach (DataRow row in prijavaUtakmiceBusiness.SelektovanjeSvihPrijavaUtakmica().Rows)
                {
                    DateTime datumUtakmiceD = Convert.ToDateTime(row[1]);
                    string datumUtakmice = datumUtakmiceD.ToString("yyyy-MM-dd");
                    combo.Items.Add(datumUtakmice);
                }
                combo.IsEnabled = true;
            }
           
            combo.SelectedIndex = 0;
        }

        private void ComboSati_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            for(int i=0;i<=24;i++)
            {
                combo.Items.Add(i);
                if (izmenaL == true)
                {
                    if (i == vS)
                    {
                        combo.SelectedIndex = i;
                    }
                    combo.IsEnabled = false;
                }
                else
                {
                    combo.SelectedIndex = 0;
                    combo.IsEnabled = true;
                }
            }
        }

        private void ComboMinuti_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            for (int i = 0; i <= 59; i++)
            {
                combo.Items.Add(i);
                if (izmenaL == true)
                {
                    if (i == vM)
                    {
                        combo.SelectedIndex = i;
                    }
                    combo.IsEnabled = false;
                }
                else
                {
                    combo.SelectedIndex = 0;
                    combo.IsEnabled = true;
                }
            }
        }
    }
}
