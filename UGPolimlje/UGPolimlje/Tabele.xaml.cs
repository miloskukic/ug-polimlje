﻿using BusinessLayer;
using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace UGPolimlje
{



    public partial class Tabele : Window
    {
        private ClanBusiness clanBussines;
        private KategorijaTakmicenjaBusiness kategorijeBusiness;
        private OpisBusiness opisBusiness;
        private PrijavaUtakmiceBusiness prijavaUtakmiceBusiness;
        private StavkaBusiness stavkaBusiness;
        private UcestvovanjeBusiness ucestvovanjeBusiness;
        private VrstaGolubaBusiness vrstaGolubaBusiness;
        private ZapisnikBusiness zapisnikBusiness;
        private GolubBusiness golubBusiness;

        private string aktivnaTabela = null;
        private int idIzmene1 = 0, kljucRbr = 0;
        string kljucDatum = "", kljucR = "", brisanjeDatum;



        public Tabele()
        {
            InitializeComponent();
            init();

            //DataTable dt = clanBussines.SelektovanjeSvihClanova();
            //DataGridView.DataContext = dt.DefaultView;
            /* for (int i = 1; i <= 3; i++)
             {
                 var column = new DataGridTextColumn();

                 column.Header = "Coulmn" + i;
                 column.Binding = new Binding("Coulmn" + i);
                 DataGridView.Columns.Add(column);
             }


             for (int i = 0; i < 10; i++)
             {
                 DataGridView.Items.Add(new DataItem { Coulmn1 = "Text" + i, Coulmn2 = "Text" + i, Coulmn3 = "Text" + i });
             }*/
        }

 

        private void init()
        {
            clanBussines = new ClanBusiness();
            kategorijeBusiness = new KategorijaTakmicenjaBusiness();
            opisBusiness = new OpisBusiness();
            prijavaUtakmiceBusiness = new PrijavaUtakmiceBusiness();
            stavkaBusiness = new StavkaBusiness();
            ucestvovanjeBusiness = new UcestvovanjeBusiness();
            vrstaGolubaBusiness = new VrstaGolubaBusiness();
            zapisnikBusiness = new ZapisnikBusiness();
            golubBusiness = new GolubBusiness();

        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Pocetna pocetna = new Pocetna();
            pocetna.Show();
            this.Close();
        }

        public void azurirajTabelu(string aktivnaTabela)
        {
            //DataTable dt;
            switch (aktivnaTabela.ToLower())
            {
                case "Clanovi":; break;
                case "PrijaveUtakmica":; break;
                case "Ucestvovanja":; break;
                case "Kategorije":; break;
                case "Zapisnici":; break;
                case "Opisi":; break;
                case "Golubovi":; break;
                case "Stavke":; break;
                case "Vrste": break;
            }
        }

        private void start()
        {
            DataTable dt = ucestvovanjeBusiness.SelektovanjeSvihUcestvovanja();
            DataGridView.DataContext = dt.DefaultView;
        }

        private void BtnClanovi_Click(object sender, RoutedEventArgs e)
        {
            aktivnaTabela = "Clanovi";
            DataTable dt = clanBussines.SelektovanjeSvihClanova();
            Console.WriteLine(dt.ToString());
            DataGridView.AutoGenerateColumns = true;
            DataGridView.DataContext = dt.DefaultView;
            BtnEdit.IsEnabled = false;
            editIcon.Opacity = 0.4;
        }

        private void BtnPrijaveUtakmica_Click(object sender, RoutedEventArgs e)
        {
            aktivnaTabela = "PrijaveUtakmica";
            DataTable dt = prijavaUtakmiceBusiness.SelektovanjeSvihPrijavaUtakmica();
            DataGridView.DataContext = dt.DefaultView;
            BtnEdit.IsEnabled = false;
            editIcon.Opacity = 0.4;
        }

        private void BtnUcestvovanja_Click(object sender, RoutedEventArgs e)
        {
            aktivnaTabela = "Ucestvovanja";
            DataTable dt = ucestvovanjeBusiness.SelektovanjeSvihUcestvovanja();
            DataGridView.DataContext = dt.DefaultView;
            BtnEdit.IsEnabled = false;
            editIcon.Opacity = 0.4;
        }

        private void BtnKategorije_Click(object sender, RoutedEventArgs e)
        {
            aktivnaTabela = "Kategorije";
            DataTable dt = kategorijeBusiness.SelektovanjeSvihKategorijaTakmicenja();
            DataGridView.DataContext = dt.DefaultView;
            BtnEdit.IsEnabled = false;
            editIcon.Opacity = 0.4;
        }

        private void BtnZapisnici_Click(object sender, RoutedEventArgs e)
        {
            aktivnaTabela = "Zapisnici";
            DataTable dt = zapisnikBusiness.SelektovanjeSvihZapisnika();
            DataGridView.DataContext = dt.DefaultView;
            BtnEdit.IsEnabled = false;
            editIcon.Opacity = 0.4;
        }

        private void BtnOpisi_Click(object sender, RoutedEventArgs e)
        {
            aktivnaTabela = "Opisi";
            DataTable dt = opisBusiness.SelektovanjeSvihOpisa();
            DataGridView.DataContext = dt.DefaultView;
            BtnEdit.IsEnabled = false;
            editIcon.Opacity = 0.4;
        }

        private void BtnGolubovi_Click(object sender, RoutedEventArgs e)
        {
            aktivnaTabela = "Golubovi";
            DataTable dt = golubBusiness.SelektovanjeSvihGolubova();
            DataGridView.DataContext = dt.DefaultView;
            BtnEdit.IsEnabled = false;
            editIcon.Opacity = 0.4;
        }

        private void BtnStavke_Click(object sender, RoutedEventArgs e)
        {
            aktivnaTabela = "Stavke";
            DataTable dt = stavkaBusiness.SelektovanjeSvihStavki();
            DataGridView.DataContext = dt.DefaultView;
            BtnEdit.IsEnabled = false;
            editIcon.Opacity = 0.4;
        }

        private void BtnVrste_Click(object sender, RoutedEventArgs e)
        {
            aktivnaTabela = "Vrste";
            DataTable dt = vrstaGolubaBusiness.SelektovanjeSvihVrstiGolubova();
            DataGridView.DataContext = dt.DefaultView;
            BtnEdit.IsEnabled = false;
            editIcon.Opacity = 0.4;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            switch (aktivnaTabela)
            {
                case "Clanovi": Clanovi c = new Clanovi(-1); c.Show(); break;
                case "PrijaveUtakmica": PrijaveUtakmica p = new PrijaveUtakmica(0); p.Show(); break;
                case "Ucestvovanja": Ucestvovanja u = new Ucestvovanja(0); u.Show(); break;
                case "Kategorije": Kategorije k = new Kategorije(-1); k.Show(); break;
                case "Zapisnici": Zapisnici z = new Zapisnici("", -1); z.Show(); break;
                case "Opisi": Opisi o = new Opisi(-1, "",""); o.Show(); break;
                case "Golubovi": Golubovi g = new Golubovi(-1,""); g.Show(); break;
                case "Stavke": Stavke s = new Stavke(-1, "", -1); s.Show(); break;
                case "Vrste": Vrste v = new Vrste(-1); v.Show(); break;
            }
        }

        private void osveziTabelu()
        {
            DataTable dt=null;
            switch (aktivnaTabela)
            {
                case "Clanovi": dt = clanBussines.SelektovanjeSvihClanova();break;
                case "PrijaveUtakmica": dt = prijavaUtakmiceBusiness.SelektovanjeSvihPrijavaUtakmica();break;
                case "Ucestvovanja": dt = ucestvovanjeBusiness.SelektovanjeSvihUcestvovanja();break;
                case "Kategorije": dt = kategorijeBusiness.SelektovanjeSvihKategorijaTakmicenja();break;
                case "Zapisnici": dt = zapisnikBusiness.SelektovanjeSvihZapisnika();break;
                case "Opisi": dt = opisBusiness.SelektovanjeSvihOpisa();break;
                case "Golubovi": dt = golubBusiness.SelektovanjeSvihGolubova();break;
                case "Stavke": dt = stavkaBusiness.SelektovanjeSvihStavki();break;
                case "Vrste": dt = vrstaGolubaBusiness.SelektovanjeSvihVrstiGolubova();break;
            }
            DataGridView.AutoGenerateColumns = true;
            DataGridView.DataContext = dt.DefaultView; 
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            switch (aktivnaTabela)
            {
                case "Clanovi": Clanovi c = new Clanovi(idIzmene1); c.Show(); break;
                case "PrijaveUtakmica": PrijaveUtakmica p = new PrijaveUtakmica(1);p.Show(); break;
                case "Ucestvovanja": Ucestvovanja u = new Ucestvovanja(1); u.Show(); break;
                case "Kategorije": Kategorije k = new Kategorije(idIzmene1); k.Show(); break;
                case "Zapisnici": Zapisnici z = new Zapisnici(kljucDatum, kljucRbr); z.Show(); break;
                case "Opisi": Opisi o = new Opisi(kljucRbr, kljucDatum, kljucR); o.Show(); break;
                case "Golubovi": Golubovi g = new Golubovi(kljucRbr,kljucR); g.Show(); break;
                case "Stavke": Stavke s = new Stavke(kljucRbr, kljucDatum, idIzmene1); s.Show(); break;
                case "Vrste": Vrste v = new Vrste(idIzmene1); v.Show(); break;
            }
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            osveziTabelu();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            //Pocetna p = new Pocetna();
           // p.Show();
        }

        private void DataGridView_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            DataGrid gd = (DataGrid)sender;
            DataRowView row_selected = gd.SelectedItem as DataRowView;

            /*Dodato kad se promeni tabela a ostane selektovano u staroj u novoj bude null i baca gresku
            ovo to sprecava i onda radi normalno i pri promeni tabele*/

            if (row_selected != null)
            {
                switch (aktivnaTabela)
                {
                    case "Clanovi": idIzmene1 = Convert.ToInt32(row_selected[0].ToString()); break;
                    case "PrijaveUtakmica": kljucRbr = Convert.ToInt32(row_selected[0].ToString()); brisanjeDatum = Convert.ToDateTime(row_selected[1]).ToString("yyyy-MM-dd"); BtnEdit.IsEnabled = false;editIcon.Opacity = 0.4; break;
                    case "Ucestvovanja": idIzmene1 = Convert.ToInt32(row_selected[0].ToString()); kljucRbr = Convert.ToInt32(row_selected[2].ToString()); brisanjeDatum = Convert.ToDateTime(row_selected[1]).ToString("yyyy-MM-dd"); BtnEdit.IsEnabled = false; editIcon.Opacity = 0.4; break;
                    case "Kategorije": idIzmene1 = Convert.ToInt32(row_selected[0].ToString()); break;
                    case "Zapisnici": brisanjeDatum = Convert.ToDateTime(row_selected[0]).ToString("yyyy-MM-dd"); kljucDatum = row_selected[0].ToString(); kljucRbr = Convert.ToInt32(row_selected[1].ToString()); break;
                    case "Opisi": brisanjeDatum = Convert.ToDateTime(row_selected[2]).ToString("yyyy-MM-dd"); kljucDatum = row_selected[2].ToString(); kljucRbr = Convert.ToInt32(row_selected[1].ToString()); kljucR=row_selected[0].ToString(); break;
                    case "Golubovi":kljucRbr = Convert.ToInt32(row_selected[1].ToString()); kljucR = row_selected[0].ToString(); break;
                    case "Stavke": brisanjeDatum = Convert.ToDateTime(row_selected[0]).ToString("yyyy-MM-dd"); kljucDatum = row_selected[0].ToString(); kljucRbr = Convert.ToInt32(row_selected[1].ToString()); idIzmene1 = Convert.ToInt32(row_selected[2].ToString()); break;
                    case "Vrste": idIzmene1 = Convert.ToInt32(row_selected[0].ToString()); break;
                }
                //Console.WriteLine(row_selected[0].ToString());
                //Console.WriteLine("DATUM> " + kljucDatum + " RBR> " + kljucRbr + " GID> " + idIzmene1);
                if(!aktivnaTabela.Equals("PrijaveUtakmica") && !aktivnaTabela.Equals("Ucestvovanja")){
                    BtnEdit.IsEnabled = true;
                    editIcon.Opacity = 1;
                }
            }
            else
            {
                BtnEdit.IsEnabled = false;
                editIcon.Opacity = 0.4;
            }

        }

        private void BrisanjeElementaKlik(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Zelite da obrisete izabrani zapis? " +
                "Zapis ce biti trajno izbrisan i nije moguce njegovo vracanje. " +
                "Neki zapisi nece moci da se obrisu ako se pojavljuju u drugim tabelama.", "Potvrda brisanja", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                switch (aktivnaTabela)
                {
                    case "Clanovi": clanBussines.BrisanjeClana(idIzmene1); break;
                    case "PrijaveUtakmica": prijavaUtakmiceBusiness.BrisanjePrijaveUtakmice(kljucRbr, brisanjeDatum); break;
                    case "Ucestvovanja": ucestvovanjeBusiness.BrisanjeUcestvovanja(idIzmene1, kljucRbr, brisanjeDatum); break;
                    case "Kategorije": kategorijeBusiness.BrisanjeKategorijeTakmicenja(idIzmene1); break;
                    case "Zapisnici": zapisnikBusiness.BrisanjeZapisnika(brisanjeDatum, kljucRbr); break;
                    case "Opisi": opisBusiness.BrisanjeOpisa(kljucR, kljucRbr, brisanjeDatum); break;
                    case "Golubovi": golubBusiness.BrisanjeGoluba(kljucR); break;
                    case "Stavke": stavkaBusiness.BrisanjeStavke(brisanjeDatum, kljucRbr, idIzmene1); break;
                    case "Vrste": vrstaGolubaBusiness.BrisanjeVrsteGoluba(idIzmene1); break;
                }
            }
      
        }
    }

    internal class DataItem
    {
        public string Coulmn1 { get; set; }
        public string Coulmn2 { get; set; }
        public string Coulmn3 { get; set; }
    }
}
