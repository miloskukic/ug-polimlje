﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace WpfApp2
{
    /// <summary>
    /// Interaction logic for Pocetna.xaml
    /// </summary>
    public partial class Pocetna : Window
    {


        private DispatcherTimer dispacherTimer = new DispatcherTimer();

        private void initializeTimer()
        {
            dispacherTimer.Tick += new EventHandler(Timer1);
            dispacherTimer.Interval = new TimeSpan(1000);
            dispacherTimer.Start();
        }

        private void Timer1(object sneder, EventArgs e)
        {
            TimerText.Content = DateTime.Now.ToString("HH:mm:ss");
        }

        public Pocetna()
        {
            initializeTimer();
            InitializeComponent();
        }
    }
}
