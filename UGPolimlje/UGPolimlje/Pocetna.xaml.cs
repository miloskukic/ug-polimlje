﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace UGPolimlje
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Pocetna : Window
    {
        private readonly DispatcherTimer dispacherTimer = new DispatcherTimer();

        private void InitializeTimer()
        {
            dispacherTimer.Tick += new EventHandler(Timer1);
            dispacherTimer.Interval = new TimeSpan(1000);
            dispacherTimer.Start();
        }

        private void Timer1(object sneder, EventArgs e)
        {
            TimerText.Content = DateTime.Now.ToString("HH:mm:ss");
        }

        public Pocetna()
        {
            InitializeTimer();
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Tabele tabele = new Tabele();
            tabele.Show();
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Kreiranje_utakmice kru = new Kreiranje_utakmice();
            kru.Show();
            this.Close();
        }
    }
}
