﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;

namespace UGPolimlje
{
    /// <summary>
    /// Interaction logic for Zapisnici.xaml
    /// </summary>
    public partial class Zapisnici : Window
    {
        private PrijavaUtakmiceBusiness prijavaUtakmiceBusiness = new PrijavaUtakmiceBusiness();
        private ZapisnikBusiness zapisnikBussines = new ZapisnikBusiness();
        private bool izmenaL = false;
        private string datumL = "";
        private int idL = 0;
        bool success = false;
        bool exceptionS = true;

        public Zapisnici(string datum, int izmena)
        {
            InitializeComponent();

            if (izmena != -1 || !(datum.Equals("")))
            {
                String pol = "";
                this.Title = "Zapisnik[Izmena postojeceg]";

                DateTime datumUtakmiceD = Convert.ToDateTime(datum);
                string datumUtakmiceP = datumUtakmiceD.ToString("yyyy-MM-dd");

                foreach (DataRow row in zapisnikBussines.Selektovanje("*", " WHERE Rbr_utakmice=" + izmena + " and DatumUtakmice = \"" + datumUtakmiceP + "\"").Rows)
                {
                    TextBoxOblacnost.Text = row[2].ToString();
                    TextBoxPJVetra.Text = row[3].ToString();
                    TextBoxVazdusniPritisak.Text = row[4].ToString();
                    TextBoxTemperatura.Text = row[5].ToString();
                    TextBoxNapomenaT.Text = row[6].ToString();
                    TextBoxNapomenaS1.Text = row[7].ToString();
                    TextBoxNapomenaS2.Text = row[8].ToString();
                }
                izmenaL = true;
                datumL = datum;
                idL = izmena;
            }
            else
            {
                this.Title = "Zapisnik [Dodavanje nov]";
            }
        }

        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnPotvrdi_Click(object sender, RoutedEventArgs e)
        {

            int rbr = 0, vazdusniPritisak = 0, temperatura = 0;
            String datumUtakmice = "", oblacnost = "", pravacJVetra = "", takmicar = "", sudija1 = "", sudija2 = "";
            try
            {
                rbr = Convert.ToInt32(ComboRbr.SelectedItem.ToString());
            }
            catch (FormatException fx)
            {
                exceptionS = false;
            }

            try
            {
                datumUtakmice = ComboDatumUtakmice.SelectedItem.ToString();
            }
            catch(Exception ex)
            {
                exceptionS = false;
            }

            oblacnost = TextBoxOblacnost.Text;
            try
            {
                temperatura = Convert.ToInt32(TextBoxTemperatura.Text);
                vazdusniPritisak = Convert.ToInt32(TextBoxVazdusniPritisak.Text);
            }
            catch (FormatException fx)
            {
                exceptionS = false;
            }
            pravacJVetra = TextBoxPJVetra.Text;
            takmicar = TextBoxNapomenaT.Text;
            sudija1 = TextBoxNapomenaS1.Text;
            sudija2 = TextBoxNapomenaS2.Text;

            Zapisnik z = new Zapisnik(datumUtakmice, rbr, oblacnost, pravacJVetra, vazdusniPritisak, temperatura, takmicar, sudija1, sudija2);

            if (exceptionS)
            {
                if (izmenaL == true)
                {
                    success = zapisnikBussines.AzuriranjeZapisnika(z);
                }
                else
                {
                    success = zapisnikBussines.UnosZapisnika(z);
                }
            }
           
            this.Close();

            if (success)
            {
                if (izmenaL)
                {
                    MessageBox.Show("Uspesno editovanju zapisa", "Zapisnici", MessageBoxButton.OK, MessageBoxImage.Information);

                }
                else
                {
                    MessageBox.Show("Uspesno dodavanje novog zapisa", "Zapisnici", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            else
            {
                if (izmenaL)
                {
                    MessageBox.Show("Greska u editovanju zapisa, proverite sintaksu i moguce dupliranje zapisa", "Zapisnici", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    MessageBox.Show("Greska u dodavanju novog zapisa, proverite sintaksu i moguce dupliranje zapisa", "Zapisnici", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

        }


        private void ComboRbr_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            if (izmenaL == true)
            {
                combo.Items.Add(idL);
                combo.IsEnabled = false;
            }
            else
            {
                foreach (DataRow row in prijavaUtakmiceBusiness.SelektovanjeSvihPrijavaUtakmica().Rows)
                {
                    combo.Items.Add(row[0].ToString());
                }
                combo.IsEnabled = true;
            }
            combo.SelectedIndex = 0;
        }

        private void ComboDatumUtakmice_Loaded(object sender, RoutedEventArgs e)
        {

            var combo = sender as ComboBox;
            if (izmenaL == true)
            {
                DateTime datumUtakmiceDL = Convert.ToDateTime(datumL);
                string datumUtakmiceL = datumUtakmiceDL.ToString("yyyy-MM-dd");
                combo.Items.Add(datumUtakmiceL);
                combo.IsEnabled = false;
            }
            else
            {
                foreach (DataRow row in prijavaUtakmiceBusiness.SelektovanjeSvihPrijavaUtakmica().Rows)
                {
                    DateTime datumUtakmiceD = Convert.ToDateTime(row[1]);
                    string datumUtakmice = datumUtakmiceD.ToString("yyyy-MM-dd");
                    combo.Items.Add(datumUtakmice);
                }
                combo.IsEnabled = true;
            }
            combo.SelectedIndex = 0;

        }
    }
}
