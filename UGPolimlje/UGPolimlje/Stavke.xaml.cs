﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UGPolimlje
{
    /// <summary>
    /// Interaction logic for Zapisnici.xaml
    /// </summary>
    public partial class Stavke : Window
    {
        private GolubBusiness golubBusiness = new GolubBusiness();
        private PrijavaUtakmiceBusiness prijavaUtakmiceBusiness = new PrijavaUtakmiceBusiness();
        private StavkaBusiness stavkaBussines = new StavkaBusiness();

        private bool izmenaL = false;
        private string datumL = "";
        private int idL = 0, idGL = 0, vPS = 0, vPM = 0, vSS = 0, vSM = 0, vPVS = 0, vPVM = 0, idBoje = 0;
        bool success = false;
        bool exceptionS = true;

        public Stavke(int izmena, string datum, int idGoluba)
        {
            InitializeComponent();

            if (izmena != -1 || !(datum.Equals("") || idGoluba != 1))
            {
                this.Title = "Stavke [Izmena postojece]";
                string vremePoletanja = "", vremeSletanja = "", vremeUPV = "";
                string[] vremePoletanjaN, vremeSletanjaN, vremeUPVN;
                DateTime datumUtakmiceD = Convert.ToDateTime(datum);
                string datumUtakmiceP = datumUtakmiceD.ToString("yyyy-MM-dd");

                foreach (DataRow row in stavkaBussines.Selektovanje("*", " WHERE rbr_utakmice=" + izmena + " and datumUtakmice = \"" + datumUtakmiceP + "\"" + " and id_goluba = \"" + idGoluba + "\"").Rows)
                {
                    idBoje = getIdBoje(row[3].ToString());
                    vremePoletanja = row[4].ToString();
                    vremeSletanja = row[5].ToString();
                    vremeUPV = row[6].ToString();
                    TextBoxNapomena.Text = row[7].ToString();
                }
                izmenaL = true;
                datumL = datum;
                idL = izmena;
                idGL = idGoluba;

                vremePoletanjaN = vremePoletanja.Split(':');
                vPS = Convert.ToInt32(vremePoletanjaN[0]);
                vPM = Convert.ToInt32(vremePoletanjaN[1]);

                vremeSletanjaN = vremeSletanja.Split(':');
                vSS = Convert.ToInt32(vremeSletanjaN[0]);
                vSM = Convert.ToInt32(vremeSletanjaN[1]);

                vremeUPVN = vremeUPV.Split(':');
                vPVS = Convert.ToInt32(vremeUPVN[0]);
                vPVM = Convert.ToInt32(vremeUPVN[1]);
            }
            else
            {
                this.Title = "Stаvke [Dodavanje nove]";
            }
        }

        private int getIdBoje(string boja)
        {
            int id = 0;
            switch (boja.ToLower())
            {
                case "bela": id = 0; break;
                case "crna": id = 1; break;
                case "siva": id = 2; break;
                case "crvena": id = 3; break;
                case "plava": id = 4; break;
                case "zelena": id = 5; break;
                case "zuta": id = 6; break;
                case "narandzasta": id = 7; break;
                case "ljubicasta": id = 8; break;
                default: id = 0; break;
            }
            return id;
        }

        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void TextBoxVreneVazdusniPritisak_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void BtnPotvrdi_Click(object sender, RoutedEventArgs e)
        {
            int rbrUtakmice = 0, bojaAlkeID = 0;
            String datumUtakmice = "", bojaAlke = "", vremePoletanja = "",
            vremeSletanja = "", leteoUPV = "", napomena = "", idGoluba = "";

            try
            {
                idGoluba = ComboIdGoluba.SelectedItem.ToString();
                rbrUtakmice = Convert.ToInt32(ComboRbr.SelectedItem.ToString());
                datumUtakmice = ComboDatumUtakmice.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                exceptionS = false;
            }





            bojaAlkeID = ComboBojaAlke.SelectedIndex;
            switch (bojaAlkeID)
            {
                case 0: bojaAlke = "Bela"; break;
                case 1: bojaAlke = "Crna"; break;
                case 2: bojaAlke = "Siva"; break;
                case 3: bojaAlke = "Crvena"; break;
                case 4: bojaAlke = "Plava"; break;
                case 5: bojaAlke = "Zelena"; break;
                case 6: bojaAlke = "Zuta"; break;
                case 7: bojaAlke = "Narandzasta"; break;
                case 8: bojaAlke = "Ljubicasta"; break;
                default: bojaAlke = "Bela"; break;
            }
            vremePoletanja = ComboSatiP.SelectedItem.ToString() + ":" + ComboMinutiP.SelectedItem.ToString();
            vremeSletanja = ComboSatiS.SelectedItem.ToString() + ":" + ComboMinutiS.SelectedItem.ToString();
            leteoUPV = ComboSatiPV.SelectedItem.ToString() + ":" + ComboMinutiPV.SelectedItem.ToString();
            napomena = TextBoxNapomena.Text;
            Stavka s = new Stavka(datumUtakmice, rbrUtakmice, idGoluba, bojaAlke, vremePoletanja, vremeSletanja, leteoUPV, napomena);

            if (exceptionS)
            {
                if (izmenaL == true)
                {
                    success = stavkaBussines.AzuriranjeStavke(s);
                }
                else
                {
                    success = stavkaBussines.UnosStavke(s);
                }
            }

            this.Close();

            if (success)
            {
                if (izmenaL)
                {
                    MessageBox.Show("Uspesno editovanju zapisa", "Stavke", MessageBoxButton.OK, MessageBoxImage.Information);

                }
                else
                {
                    MessageBox.Show("Uspesno dodavanje novog zapisa", "Stavke", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            else
            {
                if (izmenaL)
                {
                    MessageBox.Show("Greska u editovanju zapisa, proverite sintaksu i moguce dupliranje zapisa", "Stavke", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    MessageBox.Show("Greska u dodavanju novog zapisa, proverite sintaksu i moguce dupliranje zapisa", "Stavke", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

        }

        private void ComboIdGoluba_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            if (izmenaL == true)
            {
                combo.Items.Add(idGL);
                combo.IsEnabled = false;
            }
            else
            {
                foreach (DataRow row in golubBusiness.SelektovanjeSvihGolubova().Rows)
                {
                    combo.Items.Add(row[0].ToString());
                }
                combo.IsEnabled = true;
            }
            combo.SelectedIndex = 0;
        }

        private void ComboBojaAlke_Loaded(object sender, RoutedEventArgs e)
        {
            if (izmenaL == true)
            {
                var combo = sender as ComboBox;
                combo.SelectedIndex = idBoje;
                // Console.WriteLine("IDBOJE>"+idBoje);
            }

        }

        private void ComboRbr_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            if (izmenaL == true)
            {
                combo.Items.Add(idL);
                combo.IsEnabled = false;
            }
            else
            {
                foreach (DataRow row in prijavaUtakmiceBusiness.SelektovanjeSvihPrijavaUtakmica().Rows)
                {
                    combo.Items.Add(row[0].ToString());
                }
                combo.IsEnabled = true;
            }
            combo.SelectedIndex = 0;
        }

        private void ComboDatumUtakmice_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            if (izmenaL == true)
            {
                DateTime datumUtakmiceDL = Convert.ToDateTime(datumL);
                string datumUtakmiceL = datumUtakmiceDL.ToString("yyyy-MM-dd");
                combo.Items.Add(datumUtakmiceL);
                combo.IsEnabled = false;
            }
            else
            {
                foreach (DataRow row in prijavaUtakmiceBusiness.SelektovanjeSvihPrijavaUtakmica().Rows)
                {
                    DateTime datumUtakmiceD = Convert.ToDateTime(row[1]);
                    string datumUtakmice = datumUtakmiceD.ToString("yyyy-MM-dd");
                    combo.Items.Add(datumUtakmice);
                }
                combo.IsEnabled = true;
            }
            combo.SelectedIndex = 0;
        }

        private void ComboSatiP_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;

            for (int i = 0; i <= 24; i++)
            {
                combo.Items.Add(i);
                if (izmenaL == true)
                {
                    if (i == vPS)
                    {
                        combo.SelectedIndex = i;
                    }
                }
                else
                {
                    combo.SelectedIndex = 0;
                }
            }

        }



        private void ComboMinutiP_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            for (int i = 0; i <= 59; i++)
            {
                combo.Items.Add(i);
                if (izmenaL == true)
                {
                    if (i == vPM)
                    {
                        combo.SelectedIndex = i;
                    }
                }
                else
                {
                    combo.SelectedIndex = 0;
                }
            }

        }

        private void ComboSatiS_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            for (int i = 0; i <= 24; i++)
            {
                combo.Items.Add(i);
                if (izmenaL == true)
                {
                    if (i == vSS)
                    {
                        combo.SelectedIndex = i;
                    }
                }
                else
                {
                    combo.SelectedIndex = 0;
                }
            }
        }

        private void ComboMinutiS_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            for (int i = 0; i <= 59; i++)
            {
                combo.Items.Add(i);
                if (izmenaL == true)
                {
                    if (i == vSM)
                    {
                        combo.SelectedIndex = i;
                    }
                }
                else
                {
                    combo.SelectedIndex = 0;
                }
            }
        }

        private void ComboSatiPV_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            for (int i = 0; i <= 24; i++)
            {
                combo.Items.Add(i);
                if (izmenaL == true)
                {
                    if (i == vPVS)
                    {
                        combo.SelectedIndex = i;
                    }
                }
                else
                {
                    combo.SelectedIndex = 0;
                }
            }
        }

        private void ComboMinutiPV_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            for (int i = 0; i <= 59; i++)
            {
                combo.Items.Add(i);
                if (izmenaL == true)
                {
                    if (i == vPVM)
                    {
                        combo.SelectedIndex = i;
                    }
                }
                else
                {
                    combo.SelectedIndex = 0;
                }
            }
        }
    }
}
